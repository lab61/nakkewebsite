
import os
from typing import Optional

from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.conf import settings
from year.models import Page
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from .models import User, Mail
from year.models import Year

# Send mails with content from the page
def send_template_email(user: User, mailId, message="", link_url="", title="", link_label=""):
    # Get page content from database
    print(f"Title in services: {title}")
    print("send_template_email triggered")
    if user.mails.filter(id=mailId).exists:
        mail_content = Mail.objects.get(id=mailId)
        # Load and render the email template with context
        current_year = Year.current_year().year
        html_message = render_to_string('accounts/mail.html', {
            'user': user,
            'subject': title or mail_content.title,
            'short_description': mail_content.short_description,
            'message': message,
            'long_description': mail_content.long_description,
            'link_url': link_url,
            'current_year': current_year,
            'button_label': link_label or mail_content.link_label or 'Klik her'
        })
        # Strip the HTML to create a plain-text version for email clients that don't support HTML
        plain_message = strip_tags(html_message)
        
        # Configure the email
        email = EmailMessage(
            subject=title or mail_content.title,
            body=html_message,
            from_email='info@nakkefestival.dk',
            to=[user.email],
        )
        
        # Attach the HTML version of the email
        email.content_subtype = 'html'  # Set the email content type to HTML

        # Send the email
        email.send()

        print("Finished sending mail")

def send_receiver_email(receiver, mailId, message="", link_url="", title="", link_label="", attachments=None):
    print(f"Title in services: {title}")
    print("send_template_email triggered")
    
    if mailId:
        mail_content = Mail.objects.get(id=mailId)
        current_year = Year.current_year().year
        
        html_message = render_to_string('accounts/mail.html', {
            'subject': title or mail_content.title,
            'short_description': mail_content.short_description,
            'message': message,
            'long_description': mail_content.long_description,
            'link_url': link_url,
            'current_year': current_year,
            'button_label': link_label or mail_content.link_label or 'Klik her'
        })
        plain_message = strip_tags(html_message)
        
        email = EmailMessage(
            subject=title or mail_content.title,
            body=html_message,
            from_email='info@nakkefestival.dk',
            to=[receiver],
        )
        email.content_subtype = 'html'
        
        # Attach files if provided
        if attachments:
            for file in attachments:
                if file:
                    email.attach(file.name, file.read(), file.content_type)
        
        email.send()
        print("Finished sending mail")


def send_sign_in_email(user: User, mailId) -> None:
    token = default_token_generator.make_token(user)
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    verification_link = f"{os.environ['EMAIL_VERIFICATION_URL']}/{uid}/{token}/"

    # Get page content from database
    print(f"User found: {user}")
    mail_content = Mail.objects.get(id=mailId)
    # Load and render the email template with context
    html_message = render_to_string('accounts/mail.html', {
        'user': user,
        'subject': mail_content.title,
        'short_description': mail_content.short_description,
        'long_description': mail_content.long_description,
        'link_url': verification_link,
        'current_year': Year.current_year()
    })
    # Strip the HTML to create a plain-text version for email clients that don't support HTML
    plain_message = strip_tags(html_message)
    
    # Configure the email
    email = EmailMessage(
        subject=mail_content.title,
        body=html_message,
        from_email='info@nakkefestival.dk',
        to=[user.email],
    )
    print("Mail prepared!")
    
    # Attach the HTML version of the email
    email.content_subtype = 'html'  # Set the email content type to HTML

    # Send the email
    email.send()
    
    print(f"Email sent to {user}")
#    send_mail(subject, '', settings.DEFAULT_FROM_EMAIL, [user.email], html_message=message)

def decode_uid(uidb64: str) -> Optional[str]:
    """Decode the base64 encoded UID."""
    try:
        return urlsafe_base64_decode(uidb64).decode()
    except (TypeError, ValueError, OverflowError) as e:
        print(f'{e = }')
        return None

def get_user_by_uid(uid: str) -> Optional[User]:
    """Retrieve user object using UID."""
    try:
        return User.objects.get(pk=uid)
    except User.DoesNotExist as e:
        print(f'{e = }')
        return None


def send_password_reset_email(user: User, mailId) -> None:
    """
    Sends a password reset email to the user.
    """
    token = default_token_generator.make_token(user)
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    reset_link = f"{os.environ['PASSWORD_RESET_URL']}/{uid}/{token}/"
    
    # Get page content from database
    print("send_custom_email triggered")
    mail_content = Mail.objects.get(id=mailId)
    print("Mail content was found!")
    # Load and render the email template with context
    html_message = render_to_string('accounts/mail.html', {
        'user': user,
        'subject': mail_content.title,
        'short_description': mail_content.short_description,
        'long_description': mail_content.long_description,
        'link_url': reset_link,
        'button_label': mail_content.link_label,
        'current_year': Year.current_year(),
    })
    # Strip the HTML to create a plain-text version for email clients that don't support HTML
    plain_message = strip_tags(html_message)
    
    # Configure the email
    email = EmailMessage(
        subject=mail_content.title,
        body=html_message,
        from_email='info@nakkefestival.dk',
        to=[user.email],
    )
    
    # Attach the HTML version of the email
    email.content_subtype = 'html'  # Set the email content type to HTML

    # Send the email
    email.send()
    
    print(f"Email sent to {user}")
