def custom_userinfo(claims, user):
    # Basic claims
    claims['sub'] = str(user.id)  # 'sub' is a unique identifier for the user
    claims['email'] = user.email  # 'email' claim
    claims['preferred_username'] = user.username  # 'preferred_username' claim

    # Additional claims
    claims['name'] = f"{user.first_name} {user.last_name}"  # 'name' claim
    
    # Example of including groups as claims
    groups = user.groups.values_list('name', flat=True)
    claims['groups'] = list(groups)  # Optional: Send Django groups as OIDC claims

    return claims
