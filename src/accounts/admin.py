from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission
from .models import User, Committee, Mail

class UserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email', 'phone', 'birthdate')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Mail', {'fields': ('mails',)})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'phone', 'password1', 'password2', 'mails'),
        }),
    )
    list_display = ('username', 'email', 'phone', 'is_staff')
    search_fields = ('username', 'email', 'phone')
    filter_horizontal = ('committee', 'groups', 'user_permissions')

class MailAdmin(admin.ModelAdmin):
    fieldsets = [
            (None, {'fields': ('title', 'short_description', 'long_description', 'link')})
            ]

admin.site.register(User, UserAdmin)
admin.site.register(Permission)
admin.site.register(Mail, MailAdmin)
