from django.db import models
from django.contrib.auth.models import AbstractUser
from ckeditor.fields import RichTextField
from datetime import datetime, time, timedelta

class Committee(models.Model):
    name = models.CharField(max_length=50, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Address(models.Model):
    street = models.CharField(max_length=100, blank=False, null=False)  # Require street
    number = models.CharField(max_length=16, blank=False, null=False)   # Require number
    post = models.IntegerField(blank=False, null=False)                 # Require post

class Mail(models.Model):
    name = models.CharField(max_length=250, blank=True) 
    title = models.CharField(max_length=250, blank=True)
    short_description = models.CharField(max_length=500, blank=True)
    long_description = RichTextField(blank=True)
    link = models.URLField(max_length=500, blank=True, null=True)
    link_label = models.CharField(max_length=20, blank=True)
    toggleable = models.BooleanField(default=False)
    customizable = models.BooleanField(default=False)

    def __str__(self):
        return self.name
    
    class Meta:
        permissions = [
            ("send_mail", "Can send custom mails"),
        ]

class User(AbstractUser):
    phone = models.CharField(max_length=16)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, blank=True, null=True) 
    committee = models.ManyToManyField(Committee)
    img = models.ImageField(upload_to='images/profiles/', blank=True)
    mails = models.ManyToManyField(Mail, related_name='users', blank=True) # Property to see if the user gets the mailtemplates
    birthdate = models.DateTimeField()
    REQUIRED_FIELDS = ["phone", "email"]
    
    class Meta:
        permissions = [
            ("view_allusers", "Can view all Users"),
        ]
