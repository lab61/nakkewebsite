#!/usr/bin/env python3

from django import forms
from .models import Committee, User, Address, Mail
from year.models import VolunteerApplication, Year, VolunteerArea
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse, reverse_lazy
from django.contrib.auth import authenticate
from django.db.models import F
from django.core.exceptions import ValidationError
from django.contrib.auth.password_validation import validate_password
from django.utils import timezone
from datetime import datetime as dt, timedelta

class LoginForm(forms.Form):
    username = forms.CharField(
            required=True,
            label="Email",
            max_length=200,
            widget=forms.TextInput(attrs={'placeholder': 'adresse@email.dk', 'class': 'form-control'}),
            )
    password = forms.CharField(
            required=True,
            label="Kodeord",
            min_length=10,
            widget=forms.PasswordInput(attrs={'placeholder': 'Kodeord', 'class': 'form-control'}),
            )

    def clean(self):
        cleaned_data = super().clean()
        username_or_email = cleaned_data.get('username').lower()
        password = cleaned_data.get('password')

        # Authenticate the user
        if username_or_email and password:
            try:
                # Check if the input matches an email
                user_obj = User.objects.get(email=username_or_email)
                username = user_obj.username
            except User.DoesNotExist:
                # If no user is found with that email, assume it's a username
                username = username_or_email
            
            user = authenticate(username=username, password=password)
            if user is None:
                raise forms.ValidationError("Forkerte login informationer. Ret email og/eller kodeord.")

        return cleaned_data

class MagicLinkForm(forms.Form):
    email = forms.EmailField(
            required=True,
            label="Email",
            max_length=200,
            widget=forms.TextInput(attrs={'placeholder': 'adresse@email.dk', 'class': 'form-control'}),
            )

class SignupForm(forms.Form):
    required_css_class = 'required'

    ROLE_CHOICES = [
        ('guest', 'Guest'),
        ('volunteer', 'Volunteer'),
        ('artist', 'Artist'),
    ]

    role = forms.ChoiceField(choices=ROLE_CHOICES, widget=forms.RadioSelect)
    username = forms.CharField(
        label="Kaldenavn",
        max_length=200,
        widget=forms.TextInput(attrs={'placeholder': 'Elgen'})
    )
    email = forms.EmailField(
        label="Email",
        max_length=200,
        widget=forms.EmailInput(attrs={'placeholder': 'adresse@email.dk'})
    )
    phone = forms.CharField(
        label="Telefon", 
        max_length=16,
        widget=forms.TextInput(attrs={'placeholder': '+4512345678'})
    )
    password = forms.CharField(
        label="Kodeord",
        min_length=10,
        widget=forms.PasswordInput(attrs={'placeholder': 'Kodeord', 'class': 'form-control'}),
        help_text="Kodeordet skal indeholde bogstaver, tal og specialtegn '([<!%&_.,;:-@?>])'."
    )
    repeat_password = forms.CharField(
        label="Gentag kodeord",
        min_length=10,
        widget=forms.PasswordInput(attrs={'placeholder': 'Gentag kodeord', 'class': 'form-control'}),
    )
    firstname = forms.CharField(
        label="Fornavn",
        widget=forms.TextInput(attrs={'placeholder': 'Christopher'})
    )
    lastname = forms.CharField(
        label="Efternavn",
        widget=forms.TextInput(attrs={'placeholder': 'Kristoffersen'})
    )
    street = forms.CharField(
        label="Gadenavn",
        widget=forms.TextInput(attrs={'placeholder': 'Gadevej'})
    )
    number = forms.CharField(
        label="Husnummer",
        widget=forms.TextInput(attrs={'placeholder': '12'})
    )
    post = forms.IntegerField(
        label="Postnummer",
        widget=forms.TextInput(attrs={'placeholder': '1234'})
    )
    user_comment = forms.CharField(
        label="Kommentar", 
        max_length=500, 
        widget=forms.Textarea(attrs={'placeholder': 'Baggrund og behov ifm. frivillighed'}), 
        required=False
    )
    birthdate = forms.DateTimeField(
        label="Fødselsdato",
        widget=forms.DateInput(attrs={'type': 'date'}),
        input_formats=['%Y-%m-%d']
        )
    approval = forms.BooleanField(
        required=True,
        )

    def __init__(self, *args, **kwargs):
        area = kwargs.pop('area', None)
        super(SignupForm, self).__init__(*args, **kwargs)
        
        current_year = Year.current_year()
        
        area_queryset = VolunteerArea.objects.filter(
            year=current_year,
            volunteerCount__lt=F('volunteerMax')  
        ).exclude(name='Arrangør')

        self.fields['area'] = forms.ModelChoiceField(
            queryset=area_queryset,
            label="Frivilligområder",
            required=False
        )
        
        if area:
            self.fields['area'].initial = VolunteerArea.objects.filter(pk=area).first()
        
        for field_name, field in self.fields.items():
            if field_name not in ['approval']:
                field.widget.attrs['class'] = 'form-control'

    def clean(self):
        cleaned_data = super().clean()
        
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            self.add_error('username', 'Kaldenavnet eksisterer allerede.')
        if ' ' in username:
            self.add_error('username', 'Kaldenavnet må ikke indeholde mellemrum.')

        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            self.add_error('email', 'Email eksisterer allerede.')
        
        password = cleaned_data.get("password")
        repeat_password = cleaned_data.get("repeat_password")
        
        try:
            validate_password(password)
        except ValidationError as e:
            # Add each error message to the 'password' field
            for error in e.messages:
                self.add_error('password', error)

        if password and repeat_password and password != repeat_password:
            self.add_error('repeat_password', 'Kodeordene matcher ikke.')
        
        area = self.cleaned_data.get('area')
        if area and area.volunteerCount >= area.volunteerMax:
            self.add_error('area',"Frivilligområdet har ingen åbne pladser.")

        role = cleaned_data.get("role")
        if role == 'volunteer' and not cleaned_data.get('area'):
            self.add_error('area', 'Feltet er påkrævet.')

#       birthdate = cleaned_data.get("birthdate")
#        now = timezone.now()
#        min_years_ago = now - timedelta(days = 5460)
#        if birthdate < min_years_ago:
#            self.add_error('birthdate', 'Du skal være minimum 15 år, for at være frivillig.')

        return cleaned_data

class ProfileForm(forms.ModelForm):

    def get_mail_choices():
        return [(mail.id, mail.name) for mail in Mail.objects.filter(toggleable=True)]
    
    # Fields related to the User model
    phone = forms.CharField(label="Telefon", max_length=16, required=True)  # Make phone mandatory

    # Fields related to the Address model
    street = forms.CharField(label="Gadenavn", required=True)  # Make street mandatory
    number = forms.CharField(label="Husnummer", required=True)  # Make house number mandatory
    post = forms.IntegerField(label="Postnummer", required=True)  # Make post code mandatory
    birthdate = forms.DateTimeField(
            label="Fødselsdato",
            widget=forms.DateInput(attrs={'type': 'date'}),
            input_formats=['%Y-%m-%d']
            )
    mails = forms.MultipleChoiceField(
            choices=get_mail_choices,
            widget=forms.CheckboxSelectMultiple, 
            label="Tilmeldte mailnotifikationer",
            required=False
            )

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'phone', 'img', 'street', 'number', 'post', 'birthdate', 'mails']
    
    def __init__(self, *args, **kwargs):
        user = kwargs.get('instance')
        address = user.address if user and user.address else None

        super(ProfileForm, self).__init__(*args, **kwargs)

        # Prepopulate the form with address data if the address exists
        if address:
            self.fields['street'].initial = address.street
            self.fields['number'].initial = address.number
            self.fields['post'].initial = address.post

        # Add 'form-control' class to all fields for styling
        for field_name, field in self.fields.items():
            if field_name not in ['mails']:
                field.widget.attrs['class'] = 'form-control'

    def clean_post(self):
        """Custom validation for the post field to ensure it is non-zero and valid."""
        post = self.cleaned_data.get('post')
        if post is None or post <= 0:
            raise forms.ValidationError("Please enter a valid post number.")
        return post

    def save(self, commit=True):
        user = super().save(commit=False)

        # Ensure that the address fields are not empty before saving the Address instance
        street = self.cleaned_data.get('street')
        number = self.cleaned_data.get('number')
        post = self.cleaned_data.get('post')

        if not street or not number or post is None:
            raise ValueError("All address fields must be filled before saving.")

        # Create or update the related Address model
        if user.address:
            # Update existing address
            address = user.address
        else:
            # Create a new address instance
            address = Address()

        # Set the address fields from the form data
        address.street = street
        address.number = number
        address.post = post

        if commit:
            address.save()  # Save the address first
            user.address = address  # Link the address to the user
            user.save()  # Save the user to update the foreign key relationship

        return user

class ResetPasswordForm(PasswordChangeForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['old_password'].label = "Nuværende kodeord"
        self.fields['new_password1'].label = "Nyt kodeord"
        self.fields['new_password2'].label = "Gentag nyt kodeord"

        self.fields['new_password2'].help_text = ""

        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
    
    class Meta:
        model = User

class SetPasswordForm(PasswordChangeForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['old_password'].label = "Nuværende kodeord"
        self.fields['new_password1'].label = "Nyt kodeord"
        self.fields['new_password2'].label = "Gentag nyt kodeord"

        self.fields['new_password2'].help_text = ""

        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
    
    class Meta:
        model = User

class SetNotificationsForm(forms.ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs) 

        # Configure the 'mails' field with the CheckboxSelectMultiple widget
        self.fields['mails'].widget = forms.CheckboxSelectMultiple()
        self.fields['mails'].queryset = Mail.objects.all()  # Populate with available Mail instances
        self.fields['mails'].required = False  # Avoid validation issues for empty selection

        # Set initial values for 'mails'
        if self.instance.pk:  # Check if the user instance exists
            self.fields['mails'].initial = self.instance.mails.filter(toggleable=True)

        # Apply common styling
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = User
        fields = ['mails']
