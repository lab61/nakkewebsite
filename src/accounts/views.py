from django.views import View
from django.http import HttpRequest
from django.contrib.auth.models import Group
from django.contrib.auth import logout, login, authenticate
from django.shortcuts import redirect, render
from django.views.generic import FormView
from django.urls import reverse, reverse_lazy
from .forms import LoginForm, MagicLinkForm, SignupForm, ProfileForm, ResetPasswordForm, SetPasswordForm, SetNotificationsForm
from .models import User, Address, Mail
from year.models import Year, VolunteerArea, Page, Artist, ArtistApplication, VolunteerApplication
from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseRedirect
from django.template.loader import get_template
from django.views.generic import TemplateView, UpdateView, DeleteView, DetailView
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404
from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth import update_session_auth_hash
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator
from .services import send_template_email, send_sign_in_email, decode_uid, get_user_by_uid, send_password_reset_email
from django.shortcuts import render
from django.contrib import messages
from django.db.models import Exists, OuterRef
from django.forms.utils import ErrorList
from django.db.models import F


def verify_email(request: HttpRequest, uidb64: str, token: str) -> HttpResponse:
    """
    Verify user email after the user clicks on the email link.
    """
    uid = decode_uid(uidb64)
    user = get_user_by_uid(uid) if uid else None

    if user and default_token_generator.check_token(user, token):
        user.has_verified_email = True
        user.save()
        login(request, user)
        return redirect('accounts:profile')

    print("Email verification failed")
    return redirect('accounts:sign_in')

class EmailSentView(TemplateView):
    template_name = "accounts/email_sent.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Retrieve the email from the query parameters
        email = self.kwargs.get('email', "Ukendt email")
        print(f"Email from EmailSentView: {email}")
        context['email'] = email 
        
        # Retrieve the page content
        page = get_object_or_404(Page, pk=38)
        
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        
        return context

class ApplicationSentView(TemplateView):
    template_name = "accounts/application_sent.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Retrieve the email from the query parameters
        email = self.kwargs.get('email', "Ukendt email")
        print(f"Email from EmailSentView: {email}")
        context['email'] = email 
        
        # Retrieve the page content
        page = get_object_or_404(Page, pk=48)
        
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        
        return context

class SendSignInEmail(View):
    def get(self, request: HttpRequest) -> HttpResponse:
        if not request.user.is_anonymous and request.user.has_verified_email:
            return redirect('accounts:profile')
        form = LoginForm()
        return render(request, 'accounts/login.html', {'form': form})

    def post(self, request: HttpRequest) -> HttpResponse:
        data = {
            'username': request.POST['email'],
            'email': request.POST['email'],
            'password': request.POST['email']
        }
        user, created = User.objects.get_or_create(
            email=data['email'],
            defaults={'username': data['email'], 'password': data['email']}
        )
        return self._send_verification_and_respond(user)

    @staticmethod
    def _send_verification_and_respond(user: User) -> HttpResponse:
        send_sign_in_email(user)
        message = (
            f"We've sent an email ✉️ to "
            f'<a href=mailto:{user.email}" target="_blank">{user.email}</a> '
            "Please check your email to verify your account"
        )
        return HttpResponse(message)

class LoginView(TemplateView):
    template_name = "accounts/login.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        role = self.request.GET.get('role', None)
        password_form = LoginForm()  # For "password login" form
        email_form = MagicLinkForm()  # For "magic link" form

        # Get page details
        page = Page.objects.get(id=40)
        context.update({
            'title': page.title,
            'short_description': page.short_description,
            'long_description': page.long_description,
            'bg': page.bg_image,
            'email_form': email_form,
            'password_form': password_form,
            'role': role,
        })
        return context

    def post(self, request, *args, **kwargs):
        tab = request.GET.get('tab', 'magic-link')

        if tab == 'magic-link':
            return self.handle_email_login(request)
        else:
            return self.handle_password_login(request)

    def handle_password_login(self, request):
        password_form = LoginForm(request.POST)
        context = self.get_context_data()

        if password_form.is_valid():
            username_or_email = password_form.cleaned_data.get("username").lower()
            password = password_form.cleaned_data.get("password")
            try:
                # Check if the input matches an email
                user_obj = User.objects.get(email=username_or_email)
                username = user_obj.username
            except User.DoesNotExist:
                # If no user is found with that email, assume it's a username
                username = username_or_email
            
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                role = request.GET.get('role', 'guest')
                if role == 'volunteer':
                    return redirect('hzd:volunteer_create')
                elif role == 'artist':
                    return redirect('hzd:artist_create')
                else:
                    return redirect('accounts:profile')
            else:
                password_form.add_error(None, "Vi kan ikke genkende din email eller kodeord.")

        context['password_form'] = password_form
        return self.render_to_response(context)

    def handle_email_login(self, request):
        email_form = MagicLinkForm(request.POST)
        email = request.POST.get('email')
        if email:
            user = User.objects.filter(email=email).first()
            if user:
                try:
                    send_sign_in_email(
                        user=user,
                        mailId=3
                    )
                    return redirect(reverse('accounts:email_sent', kwargs={'pk': user.pk}))
                except Exception as e:
                    return HttpResponse('There was an error sending the email or redirecting. Please try again later.')
            else:
                email_form.add_error('email', "Denne email findes ikke. Prøv igen.")
        else:
            email_form.add_error('email', "Indtast en gyldig email.")

        context = self.get_context_data()
        context['email_form'] = email_form
        return self.render_to_response(context)

    def http_method_not_allowed(self, request, *args, **kwargs):
        return HttpResponseNotAllowed(["GET", "POST"])

class SignupView(TemplateView):
    template_name = "accounts/signup.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['area'] = self.request.GET.get('area')
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        role = self.request.GET.get('role', 'guest')
        area = self.request.GET.get('area', None)

        # Use the form from kwargs if it exists (i.e., after a POST), or initialize it
        form = kwargs.get('form', SignupForm(area=area))

        page = Page.objects.get(id=39)
        context.update({
            'title': page.title,
            'short_description': page.short_description,
            'long_description': page.long_description,
            'bg': page.bg_image,
            'form': form,
            'role': role,
        })
        return context

    def post(self, request, *args, **kwargs):
        role = request.GET.get('role', 'guest')
        area = request.GET.get('area', None)

        form = SignupForm(request.POST, area=area)
        context = self.get_context_data(form=form, role=role)

        if form.is_valid():
            if area:
                area = form.cleaned_data.get('area')  # Get the area from cleaned_data
                if area and area.volunteerCount >= area.volunteerMax:
                    form.add_error('area', "Frivilligområdet har ingen åbne pladser.")
            user = self.create_user(form, area)
            if user:
                send_sign_in_email(
                    user=user,
                    mailId=2
                )
                return self.safe_redirect('accounts:email_sent', pk=user.pk)

        # Render the template with the context containing errors if form is invalid
        return self.render_to_response(context)

    def create_user(self, form, area):
        """
        Create the user and any related models such as Address or VolunteerApplication.
        """
        address = self.create_address(form)

        user = User.objects.create_user(
            username=form.cleaned_data["username"].lower(),
            email=form.cleaned_data["email"].lower(),
            password=form.cleaned_data["password"],
            first_name=form.cleaned_data["firstname"],
            last_name=form.cleaned_data["lastname"],
            phone=form.cleaned_data["phone"],
            birthdate=form.cleaned_data["birthdate"],
        )

        if address:
            user.address = address
            user.save()

        role = form.cleaned_data.get('role', 'guest')
        if role == 'volunteer' and area:
            self.create_volunteer_application(user, form, area)
        
        all_mails = Mail.objects.all()
        if all_mails:
            print(f"all_mails: {all_mails}")
            user.mails.set(all_mails)

        return user

    def create_address(self, form):
        """
        Create an Address instance if address fields are provided in the form.
        """
        address_data = {
            'street': form.cleaned_data.get('street', ''),
            'number': form.cleaned_data.get('number', ''),
            'post': form.cleaned_data.get('post', ''),
        }
        address_data = {k: v for k, v in address_data.items() if v}
        if address_data:
            return Address.objects.create(**address_data)
        return None

    def create_volunteer_application(self, user, form, area):
        """
        Create a VolunteerApplication if the role is volunteer.
        """
        curYear = Year.current_year()
        user_comment = form.cleaned_data.get('user_comment', '')
        volunteer_application = VolunteerApplication.objects.create(
            user=user,
            area_id=area.id,
            user_comment=user_comment,
            status_id=1,
            year=curYear,
            #time=timezone.now()
        )

        # Add the user to the 'Frivillig' group
        frivillig_group, created = Group.objects.get_or_create(name='Frivillig')
        if not user.groups.filter(name='Frivillig').exists():
            user.groups.add(frivillig_group)

        # Send email to the new user
        area_name = VolunteerArea.objects.get(id=area.id).name
        area_group = Group.objects.get(name=area_name)
        organiser_group = Group.objects.get(name='Arrangør')
        organisers = User.objects.filter(groups=organiser_group).filter(groups=area_group)
        try:
            user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
            send_template_email(
                mailId=8,
                user=user,
                link_url=user_link,
            )
            print(f"Email sent to {user}!")
            org_link = self.request.build_absolute_uri(reverse('hzd:volunteer', kwargs={'pk': volunteer_application.pk}))
            for user in organisers:
                send_template_email(
                    mailId=14,
                    user=user,
                    link_url=org_link,
                )
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        except Exception as e:
            # Log or print the actual error message
            print(f"Error sending email: {e}")

    def safe_redirect(self, url_name, **kwargs):
        """
        Safely redirect to a URL defined by its name with optional kwargs.
        """
        try:
            return redirect(reverse(url_name, kwargs=kwargs))
        except Exception as e:
            # Log the exception if needed and show a message to the user
            # Example: logger.error(f"Redirect failed: {e}")
            messages.error(self.request, "An error occurred while redirecting. Please try again.")
            return HttpResponse('There was an error sending the email or redirecting. Please try again later.')

    def get_success_url(self):
        """
        Determine the success URL based on the user's role.
        """
        role = self.request.GET.get('role', 'guest')
        if role == 'artist':
            return reverse_lazy('hzd:artist_create')
        else:
            return reverse_lazy('accounts:profile')

class ProfileView(LoginRequiredMixin, TemplateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "accounts/profile.html"
    context_object_name = 'profile'
    model = User

    def get_queryset(self):
        userId = self.request.user_id
        return User.objects.get(id=userId)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        page = Page.objects.get(id=35)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description

        # Get the current year
        current_year = Year.current_year()
        context['current_year'] = current_year

        # Check if the user has any volunteer applications for the current year
        has_current_year_volunteer_application = VolunteerApplication.objects.filter(
            user=self.request.user, 
            year=current_year
        ).exists()
        context['has_current_year_volunteer_application'] = has_current_year_volunteer_application

        # Get the latest VolunteerApplication for the user in the current year with status "2. Godkendt"
        volunteer_application = VolunteerApplication.objects.filter(
            user=self.request.user, 
            year=current_year,
        ).order_by('-time').first()
        context['volunteer_application'] = volunteer_application 

        # Get VolunteerApplications for the user and order them by their application time
        context['volunteer_applications'] = VolunteerApplication.objects.filter(
                user=self.request.user
                ).order_by('-time').order_by('-year')
        
        # Get the latest artists for the current user including their applications
        artist_application = ArtistApplication.objects.filter(
            artist__user=self.request.user, 
            year=current_year,
        ).order_by('-time').first()
        context['artist_application'] = artist_application

        # Get Artists for the user and their applications for the current year
        context['artists'] = Artist.objects.prefetch_related('artist_applications').filter(
            user=self.request.user
        ).annotate(has_current_year_application=Exists(
            ArtistApplication.objects.filter(artist=OuterRef('pk'), year=current_year)
        ))

        # Assign the Artist properties
        if artist_application:
            context['artist'] = artist_application.artist
            context['artist_name'] = artist_application.artist.name
            context['artist_id'] = artist_application.artist.pk
            context['application'] = artist_application 
        
        # Assign the VolunteerApplication properties
        if volunteer_application:
            context['area_name'] = volunteer_application.area.name
            context['volunteer_id'] = volunteer_application.pk 
        
        # Filter areas to the current year and groups they are member of and exclude 'arrangør'
        current_year = Year.current_year()
        
        # Filter areas based on availability and exclude 'Arrangør'
        any_open_areas = VolunteerArea.objects.filter(
            year=current_year,
            published=True,
            volunteerCount__lt=F('volunteerMax')  # Filter areas with space
        ).exclude(name='Arrangør').exists()
        
        context['any_open_areas'] = any_open_areas

        return context

class UserView(LoginRequiredMixin, DetailView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"
    template_name = "accounts/profile.html"
    context_object_name = 'profile'
    model = User  # This is the model we are going to query by pk

    # This will handle the user retrieval based on the pk in the URL
    def get_object(self):
        # Retrieve the user object based on the pk provided in the URL
        return get_object_or_404(User, pk=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()  # Get the user object using the pk
        context['user'] = user

        # Set title and background image
        page = Page.objects.get(id=35)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description

        # Get the current year
        current_year = Year.current_year()
        context['current_year'] = current_year

        # Get the latest VolunteerApplication for the user in the current year with status "2. Godkendt"
        volunteer_application = VolunteerApplication.objects.filter(
            user=user, 
            year=current_year,
        ).order_by('-time').first()

        # Get all VolunteerApplications for the user and order them by application time and year
        context['volunteer_applications'] = VolunteerApplication.objects.filter(
            user=user
        ).order_by('-time', '-year')

        # Get the latest artists for the current user including their applications
        artists = Artist.objects.prefetch_related('artist_applications').filter(
            user=user
        )
        first_artist = artists.first() if artists else None

        # Get all artists for the user
        context['artists'] = artists

        # Assign artist properties
        if first_artist:
            context['artist'] = [first_artist]
            context['artist_name'] = first_artist.name
            context['artist_id'] = first_artist.pk
            context['application'] = ArtistApplication.objects.filter(
                artist=first_artist
            ).order_by('-time').first()
        else:
            context['area_name'] = f"Du er ikke tilmeldt Nakkefestival {current_year}"

        # Assign VolunteerApplication properties
        if volunteer_application:
            context['area_name'] = volunteer_application.area.name
            context['volunteer_id'] = volunteer_application.pk
        else:
            context['area_name'] = f"Du er ikke tilmeldt Nakkefestival {current_year}"

        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reset Password'
        context['submit_button'] = 'Reset Password'
        context['title'] = Page.objects.get(id=35).title
        context['short_description'] = Page.objects.get(id=35).short_description
        context['long_description'] = Page.objects.get(id=35).long_description
        context['bg'] = Page.objects.get(id=35).bg_image
        return context

class ProfileEditView(LoginRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "accounts/profile_edit.html"
    context_object_name = 'profile'
    model = User
    form_class = ProfileForm

    def get_form_kwargs(self):
        # Pass the current user to the form
        kwargs = super(ProfileEditView, self).get_form_kwargs()
        return kwargs
    
    def get_object(self, queryset=None):
        # This retrieves the current logged-in user
        user = get_object_or_404(User, pk=self.request.user.pk)
        return user
    
    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('accounts:profile')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Set title and background image
        page = Page.objects.get(id=36)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        return context

class ResetPasswordView(LoginRequiredMixin, PasswordChangeView):
    form_class = ResetPasswordForm
    template_name = 'accounts/reset_password.html'
    
    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('accounts:profile')

    def form_valid(self, form):
        # Ensure that the session is updated so that the user does not get logged out
        user = form.save()
        update_session_auth_hash(self.request, user)  # Important: Prevents user from being logged out after changing password
        # Send mail to user when they change their password
        try:
            user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
            send_template_email(
                mailId=4,
                user=user,
                link_url=user_link
            )
            print(f"Email sent to {user}!")
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        except Exception as e:
            # Log or print the actual error message
            print(f"Error sending email: {e}")
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Set title and background image
        page = Page.objects.get(id=37)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        context['submit_button'] = 'Opdater'
        return context

class PasswordResetRequestView(TemplateView):
    template_name = 'accounts/password_reset_request.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = Page.objects.get(id=37)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        return context

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        user = User.objects.filter(email=email).first()
        context = self.get_context_data()

        if user:
            # Call the service to send password reset email
            send_password_reset_email(
                    user=user,
                    mailId=21)
            return redirect(reverse('accounts:email_sent', kwargs={'pk': user.pk}))
        else:
            context['error'] = 'Email address not found.'
            return self.render_to_response(context)

class ForgottenPasswordView(FormView):
    template_name = 'accounts/forgotten_password.html'
    form_class = SetPasswordForm
    success_url = reverse_lazy('accounts:profile')

    def get_user(self, uidb64):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    def get(self, request, *args, **kwargs):
        # Extract the UID and token from the URL parameters
        uidb64 = kwargs.get('uidb64')
        token = kwargs.get('token')

        # Verify the email using the provided token and uidb64
        user = self.get_user(uidb64)
        if user is not None and default_token_generator.check_token(user, token):
            # Log the user in if verification is successful
            login(request, user)
            return super().get(request, *args, **kwargs)
        else:
            # If verification fails, redirect to sign-in page or show an error
            return redirect('accounts:sign_in')

    def form_valid(self, form):
        # Save the new password and update the session
        user = self.request.user
        form.save()
        update_session_auth_hash(self.request, user)  # Prevent user from being logged out
        # Send a notification email about the password change
        try:
            if user:
                # Call the service to send password reset email
                send_password_reset_email(
                        user=user,
                        mailId=3)
                return redirect('/profil')
            else:
                context['error'] = 'Email address not found.'
                return self.render_to_response(context)
        except Exception as e:
            print(f"Error sending email: {e}")

        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reset Password'
        context['submit_button'] = 'Reset Password'
        # Set title and background image
        page = Page.objects.get(id=37)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        return context

class ProfileDeleteView(LoginRequiredMixin, DeleteView):
    model = User
    template_name = "accounts/profile_edit.html"  # Use a template for confirmation
    success_url = reverse_lazy('accounts:signup')  # Redirect after successful deletion
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    def get_object(self, queryset=None):
        return get_object_or_404(User, pk=self.request.user.pk)

    def form_valid(self, form):
        user = self.get_object()  # Get the user object that is about to be deleted

        # Send mail to the user before deletion
        try:
            user_link = self.request.build_absolute_uri(reverse('year:index'))
            send_template_email(
                mailId=6,
                user=user,
                link_url=user_link
            )
            print(f"Email sent to {user.email}!")
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        except Exception as e:
            # TODO: Log the error message
            print(f"Error sending email: {e}")
        
        current_year = Year.current_year()
        print(f"Current_year is: {current_year}")
        # Send mail to organisers of volunteer area, if the user has an approved volunteer application
        volunteer_applications = VolunteerApplication.objects.filter(user=user)
        for application in volunteer_applications:
            print(f"Application Status: {application.status}")
            print(f"Application Year: {application.year}")
            if application.year == current_year and application.status_id == 2:
                org_link = self.request.build_absolute_uri(reverse('accounts:profile'))
                area_group = Group.objects.get(name='Booking')
                organiser_group = Group.objects.get(name='Arrangør')
                organisers = User.objects.filter(groups=organiser_group).filter(groups=area_group)
                try:
                    for user in organisers:
                        send_template_email(
                            mailId=20,
                            user=user,
                            link_url=org_link
                        )
                    print(f"Email sent to {user.email}!")
                except Exception as e:
                    # Log or print the actual error message
                    print(f"Error sending email: {e}")

        # Send mail to Booking organisers, if the user is the last owner of an approved artist
        artists = Artist.objects.filter(user=user)
        for artist in artists:
            artist_owners = artist.user.all()
            if artist_owners.count() == 1:
                print("Condition passed!")
                artist_applications = ArtistApplication.objects.filter(artist=artist)
                for application in artist_applications:
                    print(f"Application Status: {application.status}")
                    print(f"Application Year: {application.year}")
                    if application.year == current_year and application.status_id == 2:
                        org_link = self.request.build_absolute_uri(reverse('hzd:artist', kwargs={'pk': application.artist_id}))
                        area_group = Group.objects.get(name='Booking')
                        organiser_group = Group.objects.get(name='Arrangør')
                        organisers = User.objects.filter(groups=organiser_group).filter(groups=area_group)
                        try:
                            for user in organisers:
                                send_template_email(
                                    mailId=17,
                                    user=user,
                                    link_url=org_link
                                )
                            print(f"Email sent to {user.email}!")
                        except Exception as e:
                            # Log or print the actual error message
                            print(f"Error sending email: {e}")

        # Delete the user after sending the email
        response = super().form_valid(form)

        return response  # Redirect after deletion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reset Password'
        context['submit_button'] = 'Reset Password'
        # Set title and background image
        page = Page.objects.get(id=37)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        return context

class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect("/")  # Redirect to home or any success page

class AuthStatusView(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponse("Not logged in.")
        return HttpResponse(f"Logged in as {request.user.username}")

class SetNotificationsView(LoginRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = 'accounts/notifications.html'
    context_object_name = 'user'
    model = User
    form_class = SetNotificationsForm

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('accounts:profile')

    def get_form_kwargs(self):
        # Pass the current user to the form
        kwargs = super(SetNotificationsView, self).get_form_kwargs()
        return kwargs
    
    def get_object(self, queryset=None):
        # This retrieves the current logged-in user
        user = get_object_or_404(User, pk=self.request.user.pk)
        return user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Set title and background image
        page = Page.objects.get(id=35)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Gem'
        return context


