#!/usr/bin/env python3

from django.urls import path
from .views import SignupView, LoginView, LogoutView, AuthStatusView, ProfileView, ProfileEditView, ProfileDeleteView, ResetPasswordView, UserView, verify_email, SendSignInEmail, EmailSentView, ForgottenPasswordView, PasswordResetRequestView, SetNotificationsView, ApplicationSentView

urlpatterns = [
    path('opret/', SignupView.as_view(), name='signup'),
    path('log-ind/', LoginView.as_view(), name='login'),
    path('log-ud/', LogoutView.as_view(), name='logout'),
    path('auth-status/', AuthStatusView.as_view(), name='auth_status'),
    path('ret/', ProfileEditView.as_view(), name='profile_edit'),
    path('slet/', ProfileDeleteView.as_view(), name='profile_delete'),
    path('', ProfileView.as_view(), name='profile'),
    path('skift-kode/', ResetPasswordView.as_view(), name='reset_password'),
    path('<int:pk>/', UserView.as_view(), name='user'),
    path('sign-in', SendSignInEmail.as_view(), name='sign_in'),
    path('verify-email/<uidb64>/<token>/', verify_email, name='verify_email'),
    path('tilmelding-sendt/<str:email>/', ApplicationSentView.as_view(), name='application_sent'),
    path('email-sendt/<int:pk>/', EmailSentView.as_view(), name='email_sent'),
    path('ny-kode/<uidb64>/<token>/', ForgottenPasswordView.as_view(), name='forgotten_password'),
    path('ny-kode/', PasswordResetRequestView.as_view(), name='reset_request'),
    path('notifikationer/', SetNotificationsView.as_view(), name='notifications'),
]
