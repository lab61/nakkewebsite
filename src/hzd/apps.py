from django.apps import AppConfig


class HzdConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hzd'
