from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views 

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('aar/', views.YearView.as_view(), name='years'),
    path('aar/opret/', views.YearCreate.as_view(), name='year_create'),
    path('aar/tilpas/<int:pk>/', views.YearEdit.as_view(), name='year'),
    path('aar/slet/<int:pk>/', views.YearDeleteView.as_view(), name='year_delete'),
    path('booking/<int:year>/', views.BookingView.as_view(), name='bookings'),
    path('booking/opret/', views.BookingCreate.as_view(), name='booking_create'),
    path('booking/tilpas/<int:pk>/', views.BookingEdit.as_view(), name='booking'),
    path('booking/slet/<int:pk>/', views.BookingDeleteView.as_view(), name='booking_delete'),
    path('frivillige/<int:year>/', views.VolunteerView.as_view(), name='volunteers'),
    path('frivillige/opret/', views.VolunteerCreate.as_view(), name='volunteer_create'),
    path('frivillige/tilpas/<int:pk>/', views.VolunteerEdit.as_view(), name='volunteer'),
    path('frivillige/slet/<int:pk>/', views.VolunteerDeleteView.as_view(), name='volunteer_delete'),
    path('sider/', views.PageView.as_view(), name='pages'),
    path('sider/tilpas/<int:pk>/', views.PageEdit.as_view(), name='page'),
    path('sider/slet/<int:pk>/', views.PageDeleteView.as_view(), name='page_delete'),
    path('donationer/', views.DonationView.as_view(), name='donations'),
    path('donationer/opret/', views.DonationCreate.as_view(), name='donation_create'),
    path('donationer/tilpas/<int:pk>/', views.DonationEdit.as_view(), name='donation'),
    path('donationer/slet/<int:pk>/', views.DonationDeleteView.as_view(), name='donation_delete'),
    path('events/', views.EventView.as_view(), name='events'),
    path('events/create/', views.EventCreate.as_view(), name='event_create'),
    path('events/tilpas/<int:pk>/', views.EventEdit.as_view(), name='event'),
    path('events/slet/<int:pk>/', views.EventDeleteView.as_view(), name='event_delete'),
    path('kunstnere/', views.ArtistView.as_view(), name='artists'),
    path('kunstnere/opret/', views.ArtistCreate.as_view(), name='artist_create'),
    path('kunstnere/slet/<int:pk>/', views.ArtistDeleteView.as_view(), name='artist_delete'),
    path('kunstnere/tilpas/<int:pk>/', views.ArtistEdit.as_view(), name='artist'),
    path('kunstnere/profil/<int:pk>', views.ArtistProfileView.as_view(), name='artist_profile'),
    path('program/<int:year>/', views.ProgramView.as_view(), name='programs'),
    path('program/opret/', views.ProgramCreate.as_view(), name='program_create'),
    path('program/tilpas/<int:pk>/', views.ProgramEdit.as_view(), name='program'),
    path('program/slet/<int:pk>/', views.ProgramDeleteView.as_view(), name='program_delete'),
    path('omraader/<int:year>/', views.VolunteerAreaView.as_view(), name='areas'),
    path('omraader/opret/', views.VolunteerAreaCreate.as_view(), name='area_create'), 
    path('omraader/opret/<int:pk>', views.VolunteerAreaCreate.as_view(), name='area_create_with_id'), 
    path('omraader/tilpas/<int:pk>/', views.VolunteerAreaEdit.as_view(), name='area'), 
    path('omraader/slet/<int:pk>/', views.VolunteerAreaDeleteView.as_view(), name='area_delete'), 
    path('mails/', views.MailView.as_view(), name='mails'), 
    path('mails/opret/', views.MailCreateView.as_view(), name='mail_create'), 
    path('mails/tilpas/<int:pk>/', views.MailEditView.as_view(), name='mail'), 
    path('mails/slet/<int:pk>/', views.MailDeleteView.as_view(), name='mail_delete'), 
    path('send_mail/', views.SendMailView.as_view(), name='send_mail'), 
    path('vagter/<int:area>', views.VolunteerShiftView.as_view(), name='shifts'), 
    path('vagter/opret/', views.VolunteerShiftCreateView.as_view(), name='shift_create'), 
    path('vagter/tilpas/<int:pk>/', views.VolunteerShiftEditView.as_view(), name='shift'), 
    path('vagter/slet/<int:pk>/', views.VolunteerShiftDeleteView.as_view(), name='shift_delete'), 
    path('save-shift-assignments/', views.save_shift_assignments, name='save_shift_assignments'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
