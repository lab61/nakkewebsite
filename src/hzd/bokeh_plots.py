# myapp/bokeh_plots.py
from bokeh.plotting import figure, show
from bokeh.embed import components

import pandas as pd

from django.db.models import Sum
from year.models import Donation

from bokeh.io import output_notebook
from bokeh.models import ColumnDataSource

# Fetch and aggregate the data
donation_data = Donation.objects.filter(published=True).values('year__year').annotate(total_amount=Sum('amount')).order_by('year__year')


def index_volunteer():
  plot = figure(title="Frivillige", 
                x_axis_label='x', 
                y_axis_label='y', 
                sizing_mode="stretch_width", 
                tools="",
                toolbar_location=None, 
                height=200)
  plot.line([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], legend_label="Line", line_width=2)
  script, div = components(plot)
  return script, div

def index_sales():
  plot = figure(title="Billetsalg", 
                x_axis_label='x', 
                y_axis_label='y', 
                sizing_mode="stretch_width", 
                tools="",
                toolbar_location=None, 
                height=200)
  plot.line([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], legend_label="Line", line_width=2)
  script, div = components(plot)
  return script, div

def donations_plot():
  # Fetch and aggregate the data
  donation_data = Donation.objects.all().values('year__year').annotate(total_amount=Sum('amount')).order_by('year__year')

  # Convert to DataFrame for easier manipulation
  df = pd.DataFrame(donation_data)
  df.rename(columns={'year__year': 'year', 'total_amount': 'amount'}, inplace=True)
  df['amount'] = df['amount'].astype(float)

  # Create a ColumnDataSource
  source = ColumnDataSource(data=dict(years=df['year'].astype(str).tolist(), amounts=df['amount']))
  
  # Create the figure
  p = figure(x_range=df['year'].astype(str).tolist(), 
             height=400, 
             title="Total Donations per Year",
             sizing_mode="stretch_width", 
             toolbar_location=None, 
             tools="")
  
  # Add the bar glyph
  p.vbar(x='years', top='amounts', width=0.9, source=source)
  
  # Customize the plot
  p.xgrid.grid_line_color = None
  p.y_range.start = 0
  p.xaxis.axis_label = "Year"
  p.yaxis.axis_label = "Total Donation Amount"
  p.xaxis.major_label_orientation = "vertical"
  p.yaxis.formatter.use_scientific = False  # Show plain numbers instead of scientific notation


  script, div = components(p)
  return script, div
