import logging
from django.contrib.auth.models import Group
from django.views.generic import TemplateView, ListView, DetailView, UpdateView, CreateView, DeleteView, FormView 
from django.views import View
# from django.views.generic.edit import UpdateView
from django.utils import timezone
from django.shortcuts import redirect, get_object_or_404 
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from . import urls 
from accounts.models import User, Committee, Mail
from accounts.services import send_template_email
from year.models import (Year, 
                        Page, 
                        ArtistApplication,
                        ApplicationStatus,
                        VolunteerApplication, 
                        Donation, 
                        Event, 
                        Artist, 
                        Program, 
                        VolunteerArea,
                        VolunteerShift)
from .forms import (YearForm,
                    YearCreateForm,
                    PageForm, 
                    ArtistApplicationForm,
                    ArtistApplicationCreateForm,
                    VolunteerApplicationForm, 
                    VolunteerApplicationCreateForm, 
                    DonationForm, 
                    DonationCreateForm,
                    EventForm, 
                    EventCreateForm, 
                    ArtistForm,
                    ArtistCreateForm,
                    ProgramForm,
                    ProgramCreateForm,
                    VolunteerAreaForm,
                    VolunteerAreaCreateForm,
                    MailForm,
                    MailCreateForm,
                    SendMailForm,
                    VolunteerShiftCreateForm,
                    VolunteerShiftEditForm)
from .bokeh_plots import index_volunteer, index_sales, donations_plot
from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.contrib.auth.decorators import login_required, permission_required
from collections import defaultdict
from datetime import date, datetime
from django.db.models import Q 

logger = logging.getLogger(__name__) # Configure logging

class SendMailView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"
    template_name = "hzd/send_mail.html"
    form_class = SendMailForm
    permission_required = 'accounts.send_mail'
    
    def form_valid(self, form):
        # Process the form and send email
        sender = form.cleaned_data['sender']
        receiver = form.cleaned_data['receiver']
        template = form.cleaned_data['template']
        title = form.cleaned_data['title']
        message = form.cleaned_data.get('message', '')
        link = form.cleaned_data['link']
        link_label = form.cleaned_data['link_label']
         
        sender_email = sender.mail # Assign the email of the selected volunteer area

        # Assign the receiver list
        receiver_list = receiver.split(';')
        print(f"mailId: {template.id}")
        for email in receiver_list:
            print(f"address: {email}")
            if email.find('@'):
                try:
                    user = User.objects.get(email=email)
                    send_template_email(
                        user=user,
                        mailId=template.id,
                        link_url=link or template.link,
                        message=message,
                        title=title,
                        link_label=link_label
                    )
                    print(f"Email sent to {user}!")
                except Exception as e:
                    # Log or print the actual error message
                    print(f"Error sending email: {e}")
        
        return super().form_valid(form)

    def get_success_url(self):
#        next_url = self.request.GET.get('next')
#        if next_url:
        cur_year = Year.current_year()
        return reverse_lazy('hzd:volunteers', kwargs={'year': cur_year})

    def get_context_data(self, **kwargs):
        context = super(SendMailView, self).get_context_data(**kwargs)
        page = Page.objects.get(id=45)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Send mail'

        # Load form data for preview
        form = self.get_form()  # Get the form instance
        
        mail_template = form.fields['template'].initial  # Get the initial Mail template object
        if mail_template:
            short_description = mail_template.short_description
            long_description = mail_template.long_description
            link_url = mail_template.link
        else:
            short_description = "Din emnetitel her"
            long_description = ""
            link_url = 'Dit link her'

        # Pass preview data into the context
        preview_data = {
            'user': {'first_name': 'John Doe'},  # Static user data for preview purposes
            'short_description': self.request.POST.get('title', short_description),
            'long_description': self.request.POST.get('message', long_description),
            'message': self.request.POST.get('message', 'Din besked her'),
            'link_url': self.request.POST.get('link', link_url),
            'button_label': self.request.POST.get('link_label', ''),
            'current_year': Year.current_year(),
        }

        context['preview_data'] = preview_data
        return context

    def get_sender(self):
        sender_id = self.request.GET.get('sender_id') 
        if sender_id:
            try:
                area = VolunteerArea.objects.get(id=sender_id)
                if area.mail:
                    return area.mail
            except VolunteerArea.DoesNotExist:
                pass
        return None

    def get_receivers(self):
        receiver_set = set()  # Use a set to automatically handle duplicates

        # Add mail for the user with a supplied user_id
        user_id = self.request.GET.get('user_id')
        if user_id:
            try:
                user_mail = User.objects.get(id=user_id).email
                if user_mail:
                    receiver_set.add(user_mail)  # Add to set
            except User.DoesNotExist:
                pass

        # Add all emails that users in a supplied group_id
        group_id = self.request.GET.get('group_id')
        if group_id:
            try:
                group = Group.objects.get(id=group_id)
                group_mails = User.objects.filter(groups=group).values_list('email', flat=True)
                receiver_set.update(group_mails)  # Add all to set
            except Group.DoesNotExist:
                pass

        # Add all emails of users associated with the artist
        artist_id = self.request.GET.get('artist_id')
        if artist_id:
            try:
                artist = Artist.objects.get(id=artist_id)
                artist_mails = User.objects.filter(artists=artist).values_list('email', flat=True)
                receiver_set.update(artist_mails)  # Add all to set
            except Artist.DoesNotExist:
                pass

        # Add all emails of users associated with the area
        area_id = self.request.GET.get('area_id')
        if area_id:
            try:
                area = VolunteerArea.objects.get(id=area_id)
                current_year = Year.current_year()
                applications = VolunteerApplication.objects.filter(
                    area=area,
                    year=current_year,
                    status=2
                )
                area_mails = User.objects.filter(volunteer_application__in=applications).values_list('email', flat=True)
                receiver_set.update(area_mails)  # Add all to set
            except VolunteerArea.DoesNotExist:
                pass

        # Return joined list if not empty
        return ';'.join(receiver_set) if receiver_set else None

    def get_template(self):
        template_id = self.request.GET.get('template_id')
        if template_id:
            try:
                template = Mail.objects.get(id=template_id)
                return template
            except Mail.DoesNotExist:
                pass
        return None

    def get_form_kwargs(self):
        kwargs = super(SendMailView, self).get_form_kwargs()
        kwargs.update({
            'sender': self.get_sender(),
            'receiver': self.get_receivers(),
            'template': self.get_template(),
        })
        return kwargs

class IndexView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/index.html"
    permission_required = 'year.view_hzd'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        script, div = index_volunteer()
        context['bokeh_script'] = script
        context['bokeh_div'] = div

        script2, div2 = index_sales()
        context['bokeh_script2'] = script2
        context['bokeh_div2'] = div2

        context['title'] = Page.objects.get(id=16).title
        context['bg'] = Page.objects.get(id=16).bg_image
        
        return context

class YearView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/year/index.html"
    context_object_name = 'years'
    permission_required = 'year.view_year'
   
    def get_queryset(self):
        # Get the sorting parameter from the request
        sort_by = self.request.GET.get('sort', '-year')
        # Sort queryset based on the parameter
        return Year.objects.all().order_by(sort_by)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=33).title
        context['bg'] = Page.objects.get(id=33).bg_image
        return context

class YearCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/year/year.html"
    context_object_name = 'year_create'
    model = Year
    form_class = YearCreateForm
    permission_required = 'year.add_year'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:years')

    def get_form_kwargs(self):
        kwargs = super(YearCreate, self).get_form_kwargs()
        kwargs['year'] = self.get_instance()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(YearCreate, self).get_context_data(**kwargs)
        page = Page.objects.get(id=34)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        if not form.instance.year:
            form.instance.year = self.get_instance()
        return super(YearCreate, self).form_valid(form)

    def get_instance(self):
        instance_id = self.kwargs.get('year_id')
        if instance_id:
            return Year.objects.get(id=instance_id)
        return None

class YearEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/year/year.html"
    context_object_name = 'year_edit'
    model = Year
    form_class = YearForm
    permission_required = 'year.change_year'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:years')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=34).title
        context['bg'] = Page.objects.get(id=34).bg_image
        print(f"Background Image: {context['bg']}")
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        return context

class YearDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = Year
    template_name = "hzd/year/index.html"
    permission_required = 'year.delete_year'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:years')

class PageView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/pages/index.html"
    context_object_name = 'pages'
    permission_required = 'year.view_page'

    def get_queryset(self):
        # Get the sorting parameter from the request
        sort_by = self.request.GET.get('sort', 'title')
        # Sort queryset based on the parameter
        return Page.objects.all().order_by(sort_by)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=19).title
        context['bg'] = Page.objects.get(id=19).bg_image
        return context

class PageEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/pages/page.html"
    context_object_name = 'page_edit'
    model = Page
    form_class = PageForm
    permission_required = 'year.change_page'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:pages')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=20).title
        context['bg'] = Page.objects.get(id=20).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        return context

class PageDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = Page
    template_name = "hzd/pages/index.html"
    permission_required = 'year.delete_page'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:pages')

class MailView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/mails/index.html"
    context_object_name = 'mails'
    permission_required = 'accounts.view_mail'

    def get_queryset(self):
        # Get the sorting parameter from the request
        sort_by = self.request.GET.get('sort', 'title')
        # Sort queryset based on the parameter
        return Mail.objects.all().order_by(sort_by)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=42).title
        context['bg'] = Page.objects.get(id=42).bg_image
        return context

class MailCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/mails/mail.html"
    context_object_name = 'mail_create'
    model = Mail
    form_class = MailCreateForm
    permission_required = 'accounts.add_mail'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:mails')

    def get_form_kwargs(self):
        kwargs = super(MailCreateView, self).get_form_kwargs()
        kwargs['mail'] = self.get_instance()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(MailCreateView, self).get_context_data(**kwargs)
        page = Page.objects.get(id=44)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        form.instance.mail = self.get_instance()
        result = super(MailCreateView, self).form_valid(form)

        # Assign mail to all users
        all_users = User.objects.all()
        if all_users.exists():
            form.instance.users.set(all_users)
            form.instance.save()
        return result 

    def get_instance(self):
        # Assuming event is part of the URL parameters
        instance_id = self.kwargs.get('mail_id')
        if instance_id:
            return Mail.objects.get(id=instance_id)
        return None

class MailEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/mails/mail.html"
    context_object_name = 'mail_edit'
    model = Mail
    form_class = MailForm
    permission_required = 'accounts.change_mail'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:mails')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=43).title
        context['bg'] = Page.objects.get(id=43).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        return context

class MailDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = Mail
    template_name = "hzd/mails/index.html"
    permission_required = 'accounts.delete_mail'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:mails')

class BookingView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/booking/index.html"
    context_object_name = 'artist_applications'
    model = ArtistApplication
    permission_required = 'year.view_artistapplication'

    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'time')
        year_param = self.kwargs.get('year')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order
        return ArtistApplication.objects.filter(year__year=year_param).order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=17).title
        context['bg'] = Page.objects.get(id=17).bg_image
        
        # Dropdown list items are all years with volunteer applications, including the current
        every_year = list(Year.objects.filter(artist_applications__isnull=False).distinct().order_by('-year'))
        current_year_instance = Year.current_year()

        if current_year_instance not in every_year:
            every_year.insert(0, current_year_instance)

        context['every_year'] = every_year
        context['current_year'] = current_year_instance.year
        context['selected_year'] = self.kwargs.get('year')
        context['year'] = self.kwargs.get('year')
        
        # Define years_with_applications
        artist_applications = context['artist_applications']
        years_with_applications = []

        for application in artist_applications:
            application_data = {
                'year': application.year.year if application.year else None,
                'artist_name': application.artist if application.artist else None,
                'time': application.time,
                'status': application.status,
                'techrider': application.techrider,
                'user_comment': application.user_comment,
            }
            years_with_applications.append(application_data)
        
        context['years_with_applications'] = years_with_applications

        return context


class BookingEdit(LoginRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/booking/booking.html"
    context_object_name = 'booking_edit'
    model = ArtistApplication
    form_class = ArtistApplicationForm

    def get_form_kwargs(self):
        # Pass the current user to the form
        kwargs = super(BookingEdit, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_form(self, form_class=None):
        form = super(BookingEdit, self).get_form(form_class)
        return form

    def form_valid(self, form):
        artist = form.instance.artist
        #form.instance.time = timezone.now()
        response = super(BookingEdit, self).form_valid(form)
        user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
#        if form.instance.status.pk == 3:
#            # Send mail to user, that their application is declined 
#            try:
#                for user in form.instance.artist.user.all():
#                    send_template_email(
#                        mailId=8,
#                        user=user,
#                        link_url=user_link
#                    )
#                    print(f"Email sent to {user}!")
#            except Exception as e:
#                # Log or print the actual error message
#                print(f"Error sending email: {e}")
#
#        # When volunteer is approved, they are added to the group for that volunteer area
#        elif form.instance.status.pk == 2:
#            # Send mail to user, that their application is approved 
#            try:
#                for user in form.instance.artist.user.all():
#                    send_template_email(
#                        mailId=10,
#                        user=user,
#                        link_url=user_link
#                    )
#                    print(f"Email sent to {user}!")
#            except Exception as e:
#                # Log or print the actual error message
#                print(f"Error sending email: {e}")
        
        # Store the saved form instance for use in get_success_url
        self.object = form.instance

        return response

    def get_success_url(self):
        # Redirect to SendMailView if organiser
        has_perm = self.request.user.has_perm('year.change_artistapplicationstatus')
        if has_perm:
            if self.object.status.pk == 3:
                sender_id = VolunteerArea.objects.filter(name='Booking').order_by('-year').pk 
                template_id = 8 
                artist_id = self.object.artist.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&artist_id={artist_id}"
                return f"{base_url}{query_params}"
            if self.object.status.pk == 2:
                sender_id = VolunteerArea.objects.filter(name='Booking').order_by('-year').pk 
                template_id = 10 
                artist_id = self.object.artist.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&artist_id={artist_id}"
                return f"{base_url}{query_params}"

        # Redirect to next query parameter
        next_url = self.request.GET.get('next', None)
        if next_url:
            return next_url
        
        # Redirect to default page
        cur_year = Year.current_year()
        return reverse_lazy('hzd:bookings', kwargs={'year': cur_year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        page = get_object_or_404(Page, id=22)
        context['title'] = page.title
        context['bg'] = page.bg_image
        
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        
        context['artist'] = self.get_object().artist

        return context

class BookingCreate(LoginRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/booking/booking.html"
    context_object_name = 'booking_create'
    model = ArtistApplication
    form_class = ArtistApplicationCreateForm

    def get_success_url(self):
        # Redirect to SendMailView if organiser and the status is approved or declined
        has_perm = self.request.user.has_perm('year.change_artistapplicationstatus')
        if has_perm:
            if self.object.status.pk == 3:
                sender_id = VolunteerArea.objects.filter(name='Booking').order_by('-year').first().pk 
                template_id = 8 
                artist_id = self.object.artist.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&artist_id={artist_id}"
                return f"{base_url}{query_params}"
            if self.object.status.pk == 2:
                sender_id = VolunteerArea.objects.filter(name='Booking').order_by('-year').first().pk 
                template_id = 10 
                artist_id = self.object.artist.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&artist_id={artist_id}"
                return f"{base_url}{query_params}"

        # Redirect to next query parameter
        next_url = self.request.GET.get('next', None)
        if next_url:
            return next_url
        
        # Redirect to default page
        cur_year = Year.current_year()
        return reverse_lazy('hzd:bookings', kwargs={'year': cur_year})

    def get_form_kwargs(self):
        kwargs = super(BookingCreate, self).get_form_kwargs()
        # Only pass the 'artist', as 'artistapplication' is unnecessary for the form
        kwargs['artist'] = self.get_artist()
        kwargs['user'] = self.request.user
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)

        # Filter artists associated with the logged-in user
        if not self.request.user.has_perm('year.view_allartists'):
            form.fields['artist'].queryset = Artist.objects.filter(user=self.request.user)
        return form

    def form_valid(self, form):
        # Set additional fields before saving
        form.instance.year = Year.current_year()  # Ensure that Year model works as expected
        if not form.instance.status:
            form.instance.status = ApplicationStatus.objects.first()  # Ensure there's a valid status
        #form.instance.time = timezone.now()
        # Save the form
        response = super(BookingCreate, self).form_valid(form)
        
        # Send email to the new user
        area_group = Group.objects.get(name='Booking')
        organiser_group = Group.objects.get(name='Arrangør')
        organisers = User.objects.filter(groups=organiser_group).filter(groups=area_group)
        try:
            # Send mail to owners when Artist has applied
            user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
            for user in form.instance.artist.user.all():
                send_template_email(
                    mailId=7,
                    user=user,
                    link_url=user_link
                )
                print(f"Email sent to {user}!")
            # Send mail to organisers when Artist has applied
            org_link = self.request.build_absolute_uri(reverse('hzd:booking', kwargs={'pk': form.instance.pk}))
            for user in organisers:
                send_template_email(
                    mailId=5,
                    user=user,
                    link_url=user_link
                )
                print(f"Email sent to {user}!")
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        except Exception as e:
            print(f"error sending mail: {e}")
#        # When volunteer is declined, make sure they are removed from the group for that volunteer area
#        if form.instance.status_id == 3:
#            # Send mail to user, that their application is declined 
#            try:
#                # Send mail to owners when Artist has applied
#                user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
#                for user in form.instance.artist.user.all():
#                    send_template_email(
#                        mailId=8,
#                        user=user,
#                        link_url=user_link
#                    )
#                    print(f"Email sent to {user}!")
#            except Exception as e:
#                # Log or print the actual error message
#                print(f"Error sending email: {e}")
#
#        # When volunteer is approved, they are added to the group for that volunteer area
#        if form.instance.status_id == 2:
#            frivillig_group = Group.objects.get(name=area_name)
#            # Send mail to user, that their application is approved 
#            try:
#                # Send mail to owners when Artist has applied
#                user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
#                for user in form.instance.artist.user.all():
#                    send_template_email(
#                        mailId=10,
#                        user=user,
#                        link_url=user_link
#                    )
#                    print(f"Email sent to {user}!")
#            except Exception as e:
#                # Log or print the actual error message
#                print(f"Error sending email: {e}")
        
        # Store the saved form instance for use in get_success_url
        self.object = form.instance

        return response

    def get_artist(self):
        # Retrieve artist_id from the URL query parameters
        artist_id = self.request.GET.get('artist_id')
        if artist_id:
            try:
                artist = Artist.objects.get(id=artist_id)
                if self.request.user.has_perm('year.view_allartists'):
                    return artist
                if self.request.user in artist.user.all():  
                    return artist
                else:
                    raise PermissionDenied("You do not have permission to view this artist.")
            except Artist.DoesNotExist:
                return None
        return None

    def get_context_data(self, **kwargs):
        context = super(BookingCreate, self).get_context_data(**kwargs)
        # Add any additional context
        artist_id = self.request.GET.get('artist_id')
        if artist_id:
            context['artist'] = Artist.objects.get(id=artist_id)
        context['submit_button'] = 'Opret'
        return context

class BookingDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = ArtistApplication
    template_name = "hzd/booking/index.html"
    permission_required = 'year.delete_artistapplication'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        cur_year = Year.current_year()
        return reverse_lazy('hzd:bookings', kwargs={'year': cur_year})

class DonationView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/donations/index.html"
    context_object_name = 'donations'
    model = Donation
    permission_required = 'year.view_donation'

    def get_queryset(self):
        sort_by = self.request.GET.get('sort', '-year')
        return Donation.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        script, div = donations_plot()
        context['bokeh_donations_script'] = script
        context['bokeh_donations'] = div

        context['title'] = Page.objects.get(id=29).title
        context['bg'] = Page.objects.get(id=29).bg_image
        context['model'] = DonationForm()
        return context

class DonationCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/donations/donation.html"
    context_object_name = 'donation_create'
    model = Donation
    form_class = DonationCreateForm
    permission_required = 'year.add_donation'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:donations')

    def get_form_kwargs(self):
        kwargs = super(DonationCreate, self).get_form_kwargs()
        kwargs['donation'] = self.get_instance()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(DonationCreate, self).get_context_data(**kwargs)
        page = Page.objects.get(id=30)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        form.instance.donation = self.get_instance()
        return super(DonationCreate, self).form_valid(form)

    def get_instance(self):
        # Assuming event is part of the URL parameters
        instance_id = self.kwargs.get('donation_id')
        if instance_id:
            return Donation.objects.get(id=instance_id)
        return None

class DonationEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/donations/donation.html"
    context_object_name = 'donation_edit'
    model = Donation
    form_class = DonationForm
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"
    permission_required = 'year.change_donation'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:donations')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=30).title
        context['bg'] = Page.objects.get(id=30).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        return context

class DonationDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = Donation
    template_name = "hzd/donations/index.html"
    permission_required = 'year.delete_donation'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:donations')

class VolunteerView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"
    
    template_name = "hzd/volunteers/index.html"
    context_object_name = 'volunteer_applications'

    def test_func(self):
        user = self.request.user
        if user.has_perm('year.view_allareas') and user.has_perm('year.view_volunteerapplication'):
            return True

    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'time')
        year_param = self.kwargs.get('year')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order

        # Get the user's group names
        user_groups = self.request.user.groups.values_list('name', flat=True)
        # Filter VolunteerAreas where the name matches the user's group names
        volunteer_areas = VolunteerArea.objects.filter(name__in=user_groups)

        return VolunteerApplication.objects.filter(
                year__year=year_param,
                area__in=volunteer_areas
            ).order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year_param = self.kwargs.get('year')

        context['title'] = get_object_or_404(Page, id=18).title
        context['bg'] = get_object_or_404(Page, id=18).bg_image

        # Dropdown list items are all years with volunteer applications, including the current
        every_year = list(Year.objects.filter(volunteer_application__isnull=False).distinct().order_by('-year'))
        current_year_instance = Year.current_year()

        if current_year_instance not in every_year:
            every_year.insert(0, current_year_instance)

        context['every_year'] = every_year
        context['current_year'] = current_year_instance.year  # Ensure current_year is an integer year
        context['selected_year'] = year_param
        context['year'] = self.kwargs.get('year')

        # Define years_with_applications
        volunteer_applications = context['volunteer_applications']
        years_with_applications = []

        for application in volunteer_applications:
            application_data = {
                'year': application.year.year,
                'time': application.time,
                'area_name': application.area.name if application.area else None,
                'user_name': application.user.username if application.user else None,
                'status': application.status,
                'user_comment': application.user_comment,
            }
            years_with_applications.append(application_data)

        context['years_with_applications'] = years_with_applications

        return context

class VolunteerEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/volunteers/volunteer.html"
    context_object_name = 'volunteer_edit'
    model = VolunteerApplication
    form_class = VolunteerApplicationForm
    permission_required = 'year.change_volunteerapplication'
    
    def get_success_url(self):
        # Redirect to SendMailView if organiser and the application is approved or declined
        has_perm = self.request.user.has_perm('year.change_volunteerapplicationstatus')
        if has_perm:
            if self.object.status.pk == 3:
                sender_id = self.object.area.pk 
                template_id = 9
                user_id = self.object.user.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&user_id={user_id}"
                return f"{base_url}{query_params}"
            if self.object.status.pk == 2:
                sender_id = self.object.area.pk 
                template_id = 11 
                user_id = self.object.user.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&user_id={user_id}"
                return f"{base_url}{query_params}"

        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        cur_year = Year.current_year()
        return reverse_lazy('hzd:volunteers', kwargs={'year': cur_year})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Pass the current user to the form
        kwargs['user'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = Page.objects.get(id=21).title
        context['bg'] = Page.objects.get(id=21).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'

        context['user'] = self.get_object().user
        return context

    def form_valid(self, form):
        # Check if we are updating an existing instance and get its current status
        previous_status = None
        if form.instance.pk:
            previous_application = get_object_or_404(VolunteerApplication, pk=form.instance.pk)
            previous_status = previous_application.status_id

        # Save the VolunteerApplication
        response = super().form_valid(form)
        area = form.instance.area
        area_group = Group.objects.get(name=area.name)
        
#        # When the application is declined
#        if form.instance.status_id == 3:
#            if self.request.user.groups.filter(name=area.name).exists():
#                form.instance.user.groups.remove(area_group)
#            # Send mail to user, that their application is declined 
#            try:
#                user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
#                send_template_email(
#                    mailId=9,
#                    user=form.instance.user,
#                    link_url=user_link
#                )
#                print(f"Email sent to {form.instance.user}!")
#            except Exception as e:
#                print(f"Error sending email: {e}")
#
#        # When the application is approved
#        if form.instance.status_id == 2:
#            if not self.request.user.groups.filter(name=area.name).exists():
#                form.instance.user.groups.add(area_group)
#            # Send mail to user, that their application is approved 
#            try:
#                user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
#                send_template_email(
#                    mailId=11,
#                    user=form.instance.user,
#                    link_url=user_link
#                )
#            except Exception as e:
#                print(f"Error sending email: {e}")
            
        # Update Volunteer Count of Area if changed from or to Approved
        if form.instance.status_id == 2 and previous_status != 2:
            area.volunteerCount += 1
            area.save()
            print(f"Increased VolunteerCount of {area}")
        elif form.instance.status_id != 2 and previous_status == 2:
            area.volunteerCount -= 1
            area.save()
            print(f"Decreased VolunteerCount of {area}")
        
        # Store the saved form instance for use in get_success_url
        self.object = form.instance
        
        return response

class VolunteerCreate(LoginRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/volunteers/volunteer.html"
    context_object_name = 'volunteer_create'
    model = VolunteerApplication
    form_class = VolunteerApplicationCreateForm

    def get_success_url(self):
        # Redirect to SendMailView if organiser and the application is approved or declined
        has_perm = self.request.user.has_perm('year.change_volunteerapplicationstatus')
        next_url = self.request.GET.get('next')
        if has_perm:
            if self.object.status.pk == 3:
                sender_id = self.object.area.pk 
                template_id = 9
                user_id = self.object.user.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&user_id={user_id}&next={next_url}"
                return f"{base_url}{query_params}"
            if self.object.status.pk == 2:
                sender_id = self.object.area.pk 
                template_id = 12 
                user_id = self.object.user.pk
                base_url = reverse('hzd:send_mail')
                query_params = f"?sender_id={sender_id}&template_id={template_id}&user_id={user_id}&next={next_url}"
                return f"{base_url}{query_params}"

        if next_url:
            return next_url
        return reverse_lazy('hzd:volunteers')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['area'] = self.request.GET.get('area')
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = Page.objects.get(id=21).title
        context['bg'] = Page.objects.get(id=21).bg_image
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        # Set the user for the VolunteerApplication instance
        if not self.request.user.has_perm('view_allusers'):
            form.instance.user = self.request.user
        
        if not self.request.user.has_perm('change_volunteerapplicationstatus'):
            form.instance.status = ApplicationStatus.objects.get(id=1)
        
        # Return error, if area is already full
        if form.instance.area.volunteerCount >= form.instance.area.volunteerMax:
            form.add_error('area', "Området har allerede nok frivillige")
            return self.form_invalid(form)
        
        # Return error, if user already has a volunteer application from the same year
        applicationYears = []
        allUserApplications = VolunteerApplication.objects.filter(
                user=form.cleaned_data['user'],
                status=2,
                )
        for application in allUserApplications:
            applicationYears.append(application.year)
        if form.instance.year in applicationYears:
            form.add_error(None, "Brugeren er allerede tilmeldt et frivilligområde til den kommende festival")
            return self.form_invalid(form)

        form.instance.year = Year.current_year()

        # Check if we are updating an existing instance and get its current status
        previous_status = None
        if form.instance.pk:
            previous_application = get_object_or_404(VolunteerApplication, pk=form.instance.pk)
            previous_status = previous_application.status_id

        # Save the VolunteerApplication
        response = super().form_valid(form)
        
        # Add the user to the 'Frivillig' group
        frivillig_group, created = Group.objects.get_or_create(name='Frivillig')
        if not form.instance.user.groups.filter(name='Frivillig').exists():
            form.instance.user.groups.add(frivillig_group)
       
        # Send email to the new user
        area_name = form.instance.area.name
        area_group = Group.objects.get(name=area_name)
        organiser_group = Group.objects.get(name='Arrangør')
        organisers = User.objects.filter(groups=organiser_group).filter(groups=area_group)
        try:
            user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
            send_template_email(
                mailId=8,
                user=form.instance.user,
                link_url=user_link
            )
            print(f"Email sent to {form.instance.user}!")
            org_link = self.request.build_absolute_uri(reverse('hzd:volunteer', kwargs={'pk': form.instance.pk}))
            for user in organisers:
                send_template_email(
                    mailId=14,
                    user=user,
                    link_url=org_link
                )
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        except Exception as e:
            print(f"Error sending email: {e}")
        
        area = VolunteerArea.objects.get(pk=form.instance.area_id)
        
#        # When the application is declined
#        if form.instance.status_id == 3:
#            if self.request.user.groups.filter(name=area_name).exists():
#                form.instance.user.groups.remove(area_group)
#            # Send mail to user, that their application is declined 
#            try:
#                user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
#                send_template_email(
#                    mailId=9,
#                    user=form.instance.user,
#                    link_url=user_link
#                )
#                print(f"Email sent to {form.instance.user}!")
#            except Exception as e:
#                print(f"Error sending email: {e}")
#            
#        # When the application is approved
#        if form.instance.status_id == 2:
#            if not self.request.user.groups.filter(name=area_name).exists():
#                form.instance.user.groups.add(area_group)
#            # Send mail to user, that their application is approved 
#            try:
#                user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
#                send_template_email(
#                    mailId=11,
#                    user=form.instance.user,
#                    link_url=user_link
#                )
#            except Exception as e:
#                print(f"Error sending email: {e}")
            
        # Update Volunteer Count of Area if changed from or to Approved
        if form.instance.status_id == 2 and previous_status != 2:
            area.volunteerCount += 1
            area.save()
            print(f"Increased VolunteerCount of {area}")
        elif form.instance.status_id != 2 and previous_status == 2:
            area.volunteerCount -= 1
            area.save()
            print(f"Decreased VolunteerCount of {area}")
        
        # Store the saved form instance for use in get_success_url
        self.object = form.instance
        
        return response

class VolunteerDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = VolunteerApplication
    template_name = "hzd/volunteers/index.html"
    permission_required = 'year.delete_volunteerapplication'

    def get_success_url(self):
        if self.request.user.has_perm('view_allusers'):
            cur_year = Year.current_year()  # Get the current year dynamically
            return reverse_lazy('hzd:volunteers', kwargs={'year': cur_year})
        return reverse_lazy('accounts:profile')

    def post(self, request, *args, **kwargs):
        # Retrieve the object before deletion
        volunteer_application = self.get_object()
        area = volunteer_application.area

        # Check if the status is approved (id=2) before deleting
        if volunteer_application.status_id == 2:
            area.volunteerCount -= 1
            area.save()
            print(f"Decreased VolunteerCount of {area}")

        # Proceed with the deletion
        response = super().delete(request, *args, **kwargs)
        
        return response

class EventView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/events/index.html"
    context_object_name = 'events'
    permission_required = 'year.view_event'

    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'name')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order
        return Event.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=25).title
        context['bg'] = Page.objects.get(id=25).bg_image
        
        # Define years_with_applications
        events = context['events']
        eventList = []

        for event in events:
            event_data = {
                'name': event.name,
                'img': event.img,
                'short_description': event.short_description,
                'long_description': event.short_description,
            }
            eventList.append(event_data)

        context['eventList'] = eventList
        
        return context

class EventCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/events/event.html"
    context_object_name = 'event_create'
    model = Event
    form_class = EventCreateForm
    permission_required = 'year.add_event'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:events')

    def get_form_kwargs(self):
        kwargs = super(EventCreate, self).get_form_kwargs()
        kwargs['event'] = self.get_event()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EventCreate, self).get_context_data(**kwargs)
        page = Page.objects.get(id=26)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        form.instance.event = self.get_event()
        return super(EventCreate, self).form_valid(form)

    def get_event(self):
        # Assuming event is part of the URL parameters
        event_id = self.kwargs.get('event_id')
        if event_id:
            return Event.objects.get(id=event_id)
        return None

class EventEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/events/event.html"
    context_object_name = 'event_edit'
    model = Event
    form_class = EventForm
    permission_required = 'year.change_edit'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:events')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=26).title
        context['bg'] = Page.objects.get(id=26).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        context['event_edit'] = self.get_object()
        return context

class EventDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = Event
    template_name = "hzd/events/index.html"
    permission_required = 'year.delete_event'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:events')

class ArtistView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/artists/index.html"
    context_object_name = 'artists'
    permission_required = 'year.view_allartists'

    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'name')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order
        return Artist.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=27).title
        context['bg'] = Page.objects.get(id=27).bg_image
        context['current_sort'] = self.sort_by
        context['next_order'] = self.next_order
        return context

class ArtistCreate(LoginRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/artists/artist.html"
    context_object_name = 'artist_create'
    model = Artist
    form_class = ArtistCreateForm

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        if user.has_perm('view_allartists'):
            return reverse_lazy('hzd:artists')
        return reverse_lazy('accounts:profile')

    def get_form_kwargs(self):
        kwargs = super(ArtistCreate, self).get_form_kwargs()
        kwargs['artist'] = self.get_artist()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ArtistCreate, self).get_context_data(**kwargs)
        page = Page.objects.get(id=28)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        form.instance.artist = self.get_artist()
        form.instance.save()
        form.instance.user.add(self.request.user)
        
        # Create artist application
        artist_user_comment = form.cleaned_data.get('artist_user_comment')
        current_year = Year.current_year()
        artistapplication = ArtistApplication.objects.get_or_create(
                artist_id=form.instance.id,
                user_comment=artist_user_comment,
                status_id=1,
                year=current_year,
                #time=timezone.now()
                )

        # Send email to the new user
        area_group = Group.objects.get(name='Booking')
        organiser_group = Group.objects.get(name='Arrangør')
        organisers = User.objects.filter(groups=organiser_group).filter(groups=area_group)
        user_link = self.request.build_absolute_uri(reverse('accounts:profile'))
        try:
            # Send mail to owners when Artist has applied
            for user in form.instance.user.all():
                send_template_email(
                    mailId=7,
                    user=user,
                    link_url=user_link
                )
                print(f"Email sent to {user}!")
            # Send mail to organisers when Artist has applied
            org_link = self.request.build_absolute_uri(reverse('hzd:booking', kwargs={'pk': form.instance.pk}))
            for user in organisers:
                send_template_email(
                    mailId=5,
                    user=user,
                    link_url=org_link
                )
                print(f"Email sent to {user}!")
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        except Exception as e:
            print(f"error sending mail: {e}")
        
        # Add the user to the 'Frivillig' group
        artist_group, created = Group.objects.get_or_create(name='Artist')
        if not self.request.user.groups.filter(name='Artist').exists():
            self.request.user.groups.add(artist_group)
        
        return super(ArtistCreate, self).form_valid(form)
    
    def get_artist(self):
        # Assuming event is part of the URL parameters
        artist_id = self.kwargs.get('artist_id')
        if artist_id:
            return Artist.objects.get(id=artist_id)
        return None

class ArtistEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/artists/artist.html"
    context_object_name = 'artist_edit'
    model = Artist
    form_class = ArtistForm
    permission_required = 'year.change_artist'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:artists')

    def get_form_kwargs(self):
        # Pass the current user to the form
        kwargs = super(ArtistEdit, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=28).title
        context['bg'] = Page.objects.get(id=28).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        return context
    
    def form_valid(self, form):
        response = super().form_valid(form)
        # Add user to the artist group, if they are not already
        artist_group = Group.objects.get(name='Artist')
        artist_name = form.instance.name
        user_link = self.request.build_absolute_uri(reverse('hzd:artist', kwargs={'pk': form.instance.pk}))
        for user in form.instance.user.all():
            # Add User to artist group if not already
            if not user.groups.filter(name='Artist').exists():
                user.groups.add(artist_group) 

            # Send the users mails to notify them, that they were added as owners
            try:
                send_template_email(
                    mailId=15,
                    user=user,
                    link_url=user_link
                )
                print(f"Sent mail to {user}")
            except Exception as e:
                # Print the actual error message
                print(f"Error sending email: {e}")

        return super(ArtistEdit, self).form_valid(form)

class ArtistDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = Artist
    template_name = "hzd/artist/index.html"
    permission_required = 'year.delete_artist'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse_lazy('hzd:artists')

class ProgramView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/program/index.html"
    context_object_name = 'program'
    permission_required = 'year.view_program'

    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'start_time')
        year_param = self.kwargs.get('year')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order
        return Program.objects.filter(year__year=year_param).order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=31).title
        context['bg'] = Page.objects.get(id=31).bg_image
        context['current_sort'] = self.sort_by
        context['next_order'] = self.next_order
        
        # Dropdown list items are all years with volunteer applications, including the current
        every_year = list(Year.objects.filter(programs__isnull=False).distinct().order_by('-year'))
        current_year_instance = Year.current_year()

        if current_year_instance not in every_year:
            every_year.insert(0, current_year_instance)

        context['every_year'] = every_year
        context['current_year'] = current_year_instance.year
        context['selected_year'] = self.kwargs.get('year')
        context['year'] = self.kwargs.get('year')
        
        # Define years_with_applications
        programs = context['program']
        years_with_programs = []

        for program in programs:
            program_data = {
                'year': program.year.year if program.year else None,
                'stage': program.stage if program.stage else None,
                'artist': program.artist.name if program.artist else None,
                'event': program.event.name if program.event else None,
                'start_time': program.start_time,
                'end_time': program.end_time,
                'published': program.published
            }
            years_with_programs.append(program_data)

        context['years_with_programs'] = years_with_programs
        
        return context

class ProgramEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/program/program.html"
    context_object_name = 'program_edit'
    model = Program
    form_class = ProgramForm
    permission_required = 'year.change_program'
    
    def get_success_url(self):
        cur_year = Year.current_year().year
        return reverse_lazy('hzd:programs', kwargs={'year': cur_year})
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=32).title
        context['bg'] = Page.objects.get(id=32).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        return context

class ProgramCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/program/program.html"
    context_object_name = 'program_create'
    model = Program
    form_class = ProgramCreateForm
    permission_required = 'year.add_program'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        cur_year = timezone.now().year  # Get the current year dynamically
        return reverse_lazy('hzd:programs', kwargs={'year': cur_year})

    def get_form_kwargs(self):
        kwargs = super(ProgramCreate, self).get_form_kwargs()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ProgramCreate, self).get_context_data(**kwargs)
        page = Page.objects.get(id=32)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        if bool(form.instance.event) == bool(form.instance.artist):
            form.add_error(None, "Et program kan ikke både være en artist og en event.")
            return self.form_invalid(form)
        return super(ProgramCreate, self).form_valid(form)

class ProgramDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = Program
    template_name = "hzd/program/index.html"
    permission_required = 'year.delete_program'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        cur_year = Year.current_year  # Get the current year dynamically
        return reverse_lazy('hzd:programs', kwargs={'year': cur_year})

class VolunteerAreaView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"
    
    template_name = "hzd/areas/index.html"
    context_object_name = 'volunteer_areas'
    permission_required = 'year.view_volunteerarea'
    
    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'name')
        year_param = self.kwargs.get('year')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order

        return VolunteerArea.objects.filter(
                year__year=year_param,
            ).order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year_param = self.kwargs.get('year')
        context['title'] = Page.objects.get(id=23).title
        context['bg'] = Page.objects.get(id=23).bg_image
        
        # Dropdown list items are all years with volunteer applications, including the current
        every_year = list(Year.objects.filter(volunteer_area__isnull=False).distinct().order_by('-year'))
        current_year_instance = Year.current_year()

        if current_year_instance not in every_year:
            every_year.insert(0, current_year_instance)

        context['every_year'] = every_year
        context['current_year'] = current_year_instance.year
        context['selected_year'] = year_param
        context['year'] = self.kwargs.get('year')
        
        # Define years_with_applications
        volunteer_areas = context['volunteer_areas']
        years_with_areas = []

        for area in volunteer_areas:
            area_data = {
                'year': area.year.year if area.year else None,
                'name': area.name,
                'published': area.published,
                'mail': area.mail,
                'volunteerCount': area.volunteerCount,
                'volunteerMax': area.volunteerMax,
                'managerCount': area.managerCount,
                'managerMax': area.managerMax,
                'start_time': area.start_time,
                'end_time': area.end_time,
                'bg': area.bg,
                'short_description': area.short_description,
                'long_description': area.long_description
            }
            years_with_areas.append(area_data)

        context['years_with_areas'] = years_with_areas
        
        group_names = []
        groups = self.request.user.groups.all()
        for group in groups:
           group_names.append(group.name)

        context['group_names'] = group_names
        return context

class VolunteerAreaCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/areas/area.html"
    context_object_name = 'area_create'
    model = VolunteerArea
    form_class = VolunteerAreaCreateForm
    permission_required = 'year.add_volunteerarea'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        cur_year = Year.current_year()  # Get the current year dynamically
        return reverse_lazy('hzd:areas', kwargs={'year': cur_year})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Pass the relevant volunteer area to the form, if it exists
        kwargs['volunteerarea'] = self.get_instance()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            page = Page.objects.get(id=24)
            context['title'] = page.title
            context['bg'] = page.bg_image
        except Page.DoesNotExist:
            context['title'] = "Default Title"
            context['bg'] = None
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        # If your VolunteerArea model has an 'area' field, assign the instance
        instance = self.get_instance()
        if instance:
            form.instance.area = instance
        return super().form_valid(form)

    def get_instance(self):
        # Safely retrieve the related instance based on the area_id URL parameter
        instance_id = self.kwargs.get('pk')  # Will be None if area_id is not in the URL
        if instance_id:
            try:
                return VolunteerArea.objects.get(id=instance_id)
            except VolunteerArea.DoesNotExist:
                return None
        return None

class VolunteerAreaEdit(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/areas/area.html"
    context_object_name = 'area_edit'
    model = VolunteerArea
    form_class = VolunteerAreaForm
    permission_required = 'year.change_volunteerarea'

    def test_func(self):
        area = self.get_object()
        # Check if the user has the required permission
        has_permission = self.request.user.has_perm(self.permission_required)
        # Check if the user belongs to the required groups
        in_correct_group = (
            self.request.user.groups.filter(name=area.name).exists() or
            self.request.user.groups.filter(name="Frivillig Håndtering").exists()
        )
        # Return True only if both conditions are satisfied
        return has_permission and in_correct_group

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        cur_year = Year.current_year()  # Get the current year dynamically
        return reverse_lazy('hzd:areas', kwargs={'year': cur_year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=24).title
        context['bg'] = Page.objects.get(id=24).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        
        context['area_id'] = self.object.pk

        return context

class VolunteerAreaDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = VolunteerArea
    template_name = "hzd/area/index.html"
    permission_required = 'year.delete_volunteerarea'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        cur_year = Year.current_year()  # Get the current year dynamically
        return reverse_lazy('hzd:areas', kwargs={'year': cur_year})

class ArtistProfileView(LoginRequiredMixin, TemplateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/artists/profile.html"
    context_object_name = 'artistprofile'
    model = Artist

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=35).title
        context['bg'] = Page.objects.get(id=35).bg_image
        
        curUser = self.request.user

        userArtists = Artist.objects.filter(
                user = curUser
                )

        artists_with_applications = ArtistApplication.objects.filter(
                artist = artist
                ).order_by('-time')
        applications = []
        for application in years_with_applications:
            application.artist_name = application.artist.name if application.artist.name else None
            application.artist_short_description = application.artist.short_description if application.artist.short_description else None
            application.artist_long_description = application.artist.long_description if application.artist.long_description else None
            application.artist_img = application.artist.img if application.artist.img else None
            applications.append(application)
            
        context['applications'] = applications

        return context

@csrf_exempt
@login_required
@permission_required('year.change_volunteershift', raise_exception=True)
def save_shift_assignments(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            assignments = data.get('assignments', [])

            for assignment in assignments:
                shift_id = assignment.get('shift_id')
                user_id = assignment.get('user_id')  # Can be None when removing

                print(f"Received assignment: {assignment}")

                if not shift_id:
                    return JsonResponse({'success': False, 'error': 'Missing shift ID'})

                # Convert shift ID to integer
                try:
                    shift_id = int(shift_id)
                except ValueError:
                    return JsonResponse({'success': False, 'error': 'Invalid shift ID format'})

                # Ensure the shift exists
                try:
                    shift = VolunteerShift.objects.get(pk=shift_id)
                except VolunteerShift.DoesNotExist:
                    return JsonResponse({'success': False, 'error': 'Shift not found'})

                if user_id is None:
                    # **✅ Unassign the user from this shift**
                    shift.user = None
                    shift.save()
                    print(f"User removed from shift {shift_id}")
                else:
                    # Convert user_id to integer
                    try:
                        user_id = int(user_id)
                    except ValueError:
                        return JsonResponse({'success': False, 'error': 'Invalid user ID format'})

                    # Ensure the user exists
                    try:
                        user = User.objects.get(pk=user_id)
                    except User.DoesNotExist:
                        return JsonResponse({'success': False, 'error': 'User not found'})

                    # Check if the shift is already assigned
                    if shift.user:
                        return JsonResponse({'success': False, 'error': 'Shift is already assigned!'})

                    # **✅ Assign user to the shift**
                    shift.user = user
                    shift.save()
                    print(f"User {user_id} assigned to shift {shift_id}")

            return JsonResponse({'success': True})
        
        except Exception as e:
            print(f"Error encountered: {e}")
            return JsonResponse({'success': False, 'error': str(e)})

    return JsonResponse({'success': False, 'error': 'Invalid request method'})

class VolunteerShiftView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"
    
    template_name = "hzd/shifts/index.html"
    context_object_name = 'volunteer_shifts'
    permission_required = 'year.view_volunteershift'
    
    def get_queryset(self):
        area_param = self.kwargs.get('area')
        if area_param:
            queryset = VolunteerShift.objects.filter(
                area_id=area_param
            ).order_by('start_time')
        else:
            queryset = VolunteerShift.objects.none()
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=46).title
        context['bg'] = Page.objects.get(id=46).bg_image
        context['short_description'] = Page.objects.get(id=46).short_description
        context['long_description'] = Page.objects.get(id=46).long_description

        area_param = self.kwargs.get('area')
        context['area'] = VolunteerArea.objects.get(id=area_param)
        
        # Fetch all areas with the current year
        current_year = Year.current_year()
        group_names = self.request.user.groups.values_list('name', flat=True)
        every_area = list(VolunteerArea.objects.filter(
            year=current_year,
            name__in=group_names
        ).order_by('-volunteerMax'))

        context['every_area'] = every_area

        # Get approved volunteers
        approved_volunteers = VolunteerApplication.objects.filter(
            area_id=area_param, status_id=2
        )
        users = User.objects.filter(volunteer_application__in=approved_volunteers)
        context['users'] = users

        # Group shifts first by date, then by (start_time, end_time), keeping empty ones visible
        volunteer_shifts = self.get_queryset()
        grouped_shifts = defaultdict(lambda: defaultdict(list))

        for shift in volunteer_shifts:
            shift_date = shift.start_time.date()
            shift_time_range = (shift.start_time.time(), shift.end_time.time())
            grouped_shifts[shift_date][shift_time_range].append(shift)

        # Ensure all time slots are displayed even if no shifts have users
        all_time_ranges = set()
        for date, time_groups in grouped_shifts.items():
            for time_range in time_groups.keys():
                all_time_ranges.add((date, time_range))

        for date, time_range in all_time_ranges:
            if time_range not in grouped_shifts[date]:
                grouped_shifts[date][time_range] = []

        # Convert defaultdicts to regular dicts
        context['grouped_shifts'] = {date: dict(times) for date, times in grouped_shifts.items()}

        return context

class VolunteerShiftCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/shifts/shift.html"
    context_object_name = 'shift_create'
    model = VolunteerShift
    form_class = VolunteerShiftCreateForm
    permission_required = 'year.add_volunteershift'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        
        # return to the first area, that the user is a member of
        cur_year = Year.current_year()  # Get the current year dynamically
        group_names = self.request.user.groups.values_list('name', flat=True)
        area = VolunteerArea.objects.filter(name__in=group_names).first()
        return reverse_lazy('hzd:shifts', kwargs={'area': area.id})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Pass the relevant volunteer area to the form, if it exists
        kwargs['volunteerarea'] = self.request.GET.get('area_id')
        kwargs['user'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            page = Page.objects.get(id=47)
            context['title'] = page.title
            context['bg'] = page.bg_image
        except Page.DoesNotExist:
            context['title'] = "Default Title"
            context['bg'] = None
        context['submit_button'] = 'Opret'
        return context

    def form_valid(self, form):
        return super().form_valid(form)

    def get_instance(self):
        # Safely retrieve the related instance based on the area_id URL parameter
        instance_id = self.kwargs.get('area')  # Will be None if area_id is not in the URL
        if instance_id:
            try:
                return VolunteerShift.objects.get(id=instance_id)
            except VolunteerArea.DoesNotExist:
                return None
        return None

class VolunteerShiftEditView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    template_name = "hzd/shifts/shift.html"
    context_object_name = 'shift_edit'
    model = VolunteerShift
    form_class = VolunteerShiftEditForm
    permission_required = 'year.change_volunteershift'

    def test_func(self):
        shift = self.get_object()
        # Check if the user has the required permission
        has_permission = self.request.user.has_perm(self.permission_required)
        # Check if the user belongs to the required groups
        in_correct_group = (
            self.request.user.groups.filter(name=shift.area.name).exists() or
            self.request.user.groups.filter(name="Frivillig Håndtering").exists()
        )
        # Return True only if both conditions are satisfied
        return has_permission and in_correct_group

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        
        # return to the first area, that the user is a member of
        cur_year = Year.current_year()  # Get the current year dynamically
        group_names = self.request.user.groups.values_list('name', flat=True)
        area = VolunteerArea.objects.filter(name__in=group_names).first()
        return reverse_lazy('hzd:shifts', kwargs={'area': area.id})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['cur_user'] = self.request.user
        kwargs['area'] = self.request.GET.get('area_id')
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=47).title
        context['bg'] = Page.objects.get(id=47).bg_image
        context['submit_button'] = 'Opdater'
        context['button_label_delete'] = 'Slet'
        
        context['shift_id'] = self.object.pk

        return context

class VolunteerShiftDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    login_url = "/profil/log-ind/"
    redirect_field_name = "redirect_to"

    model = VolunteerShift
    template_name = "hzd/shifts/index.html"
    permission_required = 'year.delete_volunteershift'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        
        # return to the first area, that the user is a member of
        cur_year = Year.current_year()  # Get the current year dynamically
        group_names = self.request.user.groups.values_list('name', flat=True)
        area = VolunteerArea.objects.filter(name__in=group_names).first()
        return reverse_lazy('hzd:shifts', kwargs={'area': area.id})
