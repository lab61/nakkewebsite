from django import forms
from django.utils import timezone
from datetime import timedelta, date
from year.models import Year, Page, Donation, Event, Artist, Program, ApplicationStatus, ArtistApplication, Venue, Concert, Genre, ArtistManagement, VolunteerApplication, VolunteerArea, VolunteerShift
from accounts.models import User, Mail
from django.contrib.postgres.forms import SimpleArrayField
from django.utils.timezone import localtime
from django.core.exceptions import ValidationError
from django.db.models import F
from django.shortcuts import redirect, get_object_or_404 
from ckeditor.widgets import CKEditorWidget
 
class SendMailForm(forms.Form):
    required_css_class = 'required'
    
    title = forms.CharField(
            required=True,
            label="Titel"
            )
    sender = forms.ModelChoiceField(
            queryset = VolunteerArea.objects.filter(
                year = Year.current_year(),
                mail = 'info@nakkefestival.dk'
                ),
            required = True,
            label="Afsender", 
            )
    receiver = forms.CharField(
            required = True,
            label = "Modtager(e)",
            help_text = "Emails separeret af semikolon (;)"
            )
    template = forms.ModelChoiceField(
            queryset = Mail.objects.filter(customizable=True),
            required = True,
            label="Skabelon", 
            )
    message = forms.CharField(
            widget=CKEditorWidget(), 
            required=False,
            label="Besked", 
            )
    link = forms.URLField(
            required=False,
            label="Link"
            )
    link_label = forms.CharField(
            required=False,
            label="Linkets titel", 
            )
    
    def __init__(self, *args, **kwargs):
        self.sender = kwargs.pop('sender', None)
        self.receiver = kwargs.pop('receiver', None)
        self.template = kwargs.pop('template', None)
        super(SendMailForm, self).__init__(*args, **kwargs)
        
        if self.sender:
            self.fields['sender'].initial = self.sender
        if self.receiver:
            self.fields['receiver'].initial = self.receiver
        if self.template:
            self.fields['template'].initial = self.template
            self.fields['title'].initial = self.template.title
            self.fields['link'].initial = self.template.link
            self.fields['link_label'].initial = self.template.link_label

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput':
                field.widget.attrs['class'] = 'form-control'

class YearForm(forms.ModelForm):
    required_css_class = 'required'
    start_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    end_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))

    class Meta:
        model = Year
        fields = '__all__'

    def clean_end_time(self):
        start_time = self.cleaned_data.get('start_time')
        end_time = self.cleaned_data.get('end_time')

        # Check if end_time is after start_time
        if end_time <= start_time:
            raise ValidationError("Slut tidspunktet skal være efter starts tidspunktet.")
        return end_time
 
    def __init__(self, *args, **kwargs):
        super(YearForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['published'].label = "Udgivet"
        self.fields['start_time'].label = "Festivalstart"
        self.fields['end_time'].label = "Festivalslut"
        self.fields['banner_image'].label = "Banner"

        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'

class YearCreateForm(forms.ModelForm):
    required_css_class = 'required'
    start_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    end_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    
    class Meta:
        model = Year
        fields = ( 
                'year', 
                'published',
                'start_time',
                'end_time',
                'banner_image')
    
    def clean_end_time(self):
        start_time = self.cleaned_data.get('start_time')
        end_time = self.cleaned_data.get('end_time')

        # Check if end_time is after start_time
        if end_time <= start_time:
            raise ValidationError("Slut tidspunktet skal være efter starts tidspunktet.")
        return end_time
 
    def __init__(self, *args, **kwargs):
        self.Year = kwargs.pop('year', None)
        super(YearCreateForm, self).__init__(*args, **kwargs)
        
        # Set labels for visible fields
        self.fields['year'].label = "År"
        self.fields['published'].label = "Udgivet"
        self.fields['start_time'].label = "Festivalstart"
        self.fields['end_time'].label = "Festivalslut"
        self.fields['banner_image'].label = "Banner"

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput' and field_name != 'published':
                field.widget.attrs['class'] = 'form-control'

class PageForm(forms.ModelForm):
    required_css_class = 'required'
    year = forms.ModelChoiceField(queryset=Year.objects.all(), disabled=True)

    class Meta:
        model = Page
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(PageForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['title'].label = "Titel"
        self.fields['bg_image'].label = "Billede"
        self.fields['short_description'].label = "Kort beskrivelse"
        self.fields['long_description'].label = "Lang beskrivelse"
        self.fields['organiser_page'].label = "Kun for arrangører"

        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['organiser_page']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'

class MailForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Mail
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        self.Mail = kwargs.pop('mail', None)
        super(MailForm, self).__init__(*args, **kwargs)
        self.fields['title'].label = "Titel"
        self.fields['short_description'].label = "Fremhævet tekst"
        self.fields['long_description'].label = "Tekst"
        self.fields['link'].label = "Link"
        self.fields['toggleable'].label = "Kan slås fra af brugeren"
        self.fields['customizable'].label = "Kan uddybes ved afsendelse"

        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['toggleable','customizable']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class MailCreateForm(forms.ModelForm):
    required_css_class = 'required'
    
    class Meta:
        model = Mail
        fields = '__all__' 
    
    def __init__(self, *args, **kwargs):
        self.Mail = kwargs.pop('mail', None)
        super(MailCreateForm, self).__init__(*args, **kwargs)
        
        # Set labels for visible fields
        self.fields['title'].label = "Titel"
        self.fields['short_description'].label = "Kort beskrivelse"
        self.fields['long_description'].label = "Lang beskrivelse"
        self.fields['link'].label = "Link"
        
        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput':
                field.widget.attrs['class'] = 'form-control'
 
class DonationForm(forms.ModelForm):
    required_css_class = 'required'
    class Meta:
        model = Donation
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(DonationForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['amount'].label = "Beløb"
        self.fields['receiver'].label = "Modtager"
        self.fields['phone'].label = "Telefon"
        self.fields['mail'].label = "Email"
        self.fields['link'].label = "Link"
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class DonationCreateForm(forms.ModelForm):
    required_css_class = 'required'
    
    class Meta:
        model = Donation
        fields = ( 
                'year', 
                'amount',
                'receiver',
                'published',
                'phone',
                'mail',
                'link')
    
    def __init__(self, *args, **kwargs):
        self.Donation = kwargs.pop('donation', None)
        super(DonationCreateForm, self).__init__(*args, **kwargs)
        
        # Set labels for visible fields
        self.fields['year'].label = "År"
        self.fields['amount'].label = "Beløb"
        self.fields['receiver'].label = "Modtager"
        self.fields['published'].label = "Udgivet"
        self.fields['phone'].label = "Telefon"
        self.fields['mail'].label = "Mail"
        self.fields['link'].label = "Link"
        
        # Set initial values
        self.initial['year'] = Year.current_year()

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput' and field_name != 'published':
                field.widget.attrs['class'] = 'form-control'

class EventForm(forms.ModelForm):
    required_css_class = 'required'
    class Meta:
        model = Event
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Titel"
        self.fields['img'].label = "Billede"
        self.fields['short_description'].label = "Kort beskrivelse"
        self.fields['long_description'].label = "Lang beskrivelse"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class EventCreateForm(forms.ModelForm):
    required_css_class = 'required'
    
    class Meta:
        model = Event
        fields = (
                'name', 
                'short_description', 
                'long_description', 
                'img')
    
    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event', None)
        super(EventCreateForm, self).__init__(*args, **kwargs)
        
        # Set labels for visible fields
        self.fields['name'].label = "Titel"
        self.fields['short_description'].label = "Manchét"
        self.fields['long_description'].label = "Beskrivelse"
        self.fields['img'].label = "Billede"

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput':
                field.widget.attrs['class'] = 'form-control'
    
class ArtistForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Artist
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ArtistForm, self).__init__(*args, **kwargs)

        # Set labels
        self.fields['name'].label = "Navn"
        self.fields['short_description'].label = "Kort beskrivelse"
        self.fields['long_description'].label = "Lang beskrivelse"
        self.fields['img'].label = "Billede"
        self.fields['link_youtube'].label = "YouTube-link"
        self.fields['link_spotify'].label = "Spotify-link"
        self.fields['link_soundcloud'].label = "Soundcloud-link"
        self.fields['link_bandcamp'].label = "Bandcamp-link"
        self.fields['link_other'].label = "Andet link"
        self.fields['user'].label = "Ejere"

        # Filter the users for the 'user' field
        if self.user and not self.user.has_perm('accounts.view_allusers'):
            # When editing an artist, show only users associated with this artist
            self.fields['user'].queryset = self.instance.user.all()

        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name in {'state', 'link'}:
                field.disabled = True
 
class ArtistCreateForm(forms.ModelForm):
    required_css_class = 'required'
    
    artist_user_comment = forms.CharField(label="Kommentar", widget=forms.Textarea, required=False)
    class Meta:
        model = Artist
        fields = (
                'name',
                'short_description',
                'long_description',
                'img',
                'link_youtube',
                'link_spotify',
                'link_bandcamp',
                'link_soundcloud',
                'link_other')
    
    def __init__(self, *args, **kwargs):
        self.artist = kwargs.pop('artist', None)
        self.user = kwargs.pop('user', None)

        super(ArtistCreateForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Navn"
        self.fields['short_description'].label = "Kort beskrivelse"
        self.fields['long_description'].label = "Lang beskrivelse"
        self.fields['img'].label = "Billede"
        self.fields['link_youtube'].label = "YouTube-link"
        self.fields['link_spotify'].label = "Spotify-link"
        self.fields['link_soundcloud'].label = "Soundcloud-link"
        self.fields['link_bandcamp'].label = "Bandcamp-link"
        self.fields['link_other'].label = "Andet link"
        
        if self.user and not self.user.has_perm('accounts.view_user'):
            self.fields['user'].widget = forms.HiddenInput()
        
        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput':
                field.widget.attrs['class'] = 'form-control'

    
class ProgramForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Program
        fields = '__all__'
        widgets = {
            'start_time': forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control'}),
            'end_time': forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(ProgramForm, self).__init__(*args, **kwargs)

        # Set labels
        self.fields['year'].label = "År"
        self.fields['stage'].label = "Scene"
        self.fields['artist'].label = "Kunstner"
        self.fields['event'].label = "Event"
        self.fields['start_time'].label = "Starttid"
        self.fields['end_time'].label = "Sluttid"
        self.fields['published'].label = "Udgivet"
        
        # Apply classes
        for field_name, field in self.fields.items():
            if field_name != 'published':
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'

    def save(self, commit=True):
        instance = super(ProgramForm, self).save(commit=False)
        instance.year = Year.current_year()
        if commit:
            instance.save()
        return instance
 
class ProgramCreateForm(forms.ModelForm):
    required_css_class = 'required'
    start_time = forms.DateTimeField(
            widget=forms.DateTimeInput(attrs={'type': 'datetime-local'})
            )
    end_time = forms.DateTimeField(
            widget=forms.DateTimeInput(attrs={'type': 'datetime-local'})
            )
    
    class Meta:
        model = Program
        fields = (
                'year', 
                'stage', 
                'artist',
                'event',
                'start_time',
                'end_time',
                'published')
    
    def __init__(self, *args, **kwargs):
        self.program = kwargs.pop('program', None)
        super(ProgramCreateForm, self).__init__(*args, **kwargs)
        
        # Set labels for visible fields
        self.fields['year'].label = "År"
        self.fields['stage'].label = "Scene"
        self.fields['artist'].label = "Kunstner"
        self.fields['event'].label = "Event"
        self.fields['start_time'].label = "Start"
        self.fields['end_time'].label = "Slut"
        self.fields['published'].label = "Udgivet"

        # Set initial values
        self.initial['year'] = Year.current_year()

        festivalstart = Year.current_year().start_time
        self.initial['start_time'] = festivalstart

        end_time = self.initial['start_time']+timedelta(hours=1)
        self.initial['end_time'] = end_time

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput' and field_name != 'published':
                field.widget.attrs['class'] = 'form-control'

class ArtistApplicationForm(forms.ModelForm):
    required_css_class = 'required'
    
    class Meta:
        model = ArtistApplication
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ArtistApplicationForm, self).__init__(*args, **kwargs)
        self.fields['artist'].label = "Kunstner"
        self.fields['year'].label = "År"
        self.fields['status'].label = "Status"
        self.fields['techrider'].label = "Techrider"
        self.fields['user_comment'].label = "Kommentar"
        self.fields['org_comment'].label = "Arrangørnoter"

        # Hide fields if not permitted
        if self.user and not self.user.has_perm('year.view_artistapplicationstatus'):
            del self.fields['status']
        if self.user and not self.user.has_perm('year.view_orgcomment'):
            del self.fields['org_comment']

        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if not isinstance(field.widget, forms.HiddenInput):
                field.widget.attrs['class'] = 'form-control'
            if field_name in ['year','artist','time']:
                field.disabled = True
     
class ArtistApplicationCreateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = ArtistApplication
        fields = ('artist', 'techrider', 'user_comment', 'org_comment', 'status')

    def __init__(self, *args, **kwargs):
        # Extract the 'user' from kwargs
        user = kwargs.pop('user', None)
        artist = kwargs.pop('artist', None)
        super(ArtistApplicationCreateForm, self).__init__(*args, **kwargs)

        # Set labels for visible fields
        self.fields['artist'].label = "Kunstner"
        self.fields['techrider'].label = "Techrider"
        self.fields['user_comment'].label = "Kommentar"

        # Set initial values
        self.initial['year'] = Year.current_year()

        # Perform permission checks using the passed user
        if user:
            if not user.has_perm('year.view_artistapplicationstatus'):
                if 'status' in self.fields:
                    del self.fields['status']
            if not user.has_perm('year.view_orgcomment'):
                if 'org_comment' in self.fields:
                    del self.fields['org_comment']

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput' and field_name != 'published':
                field.widget.attrs['class'] = 'form-control'

        # Pre-select the artist in the form if it's provided
        if artist:
            self.initial['artist'] = artist

class VenueForm(forms.ModelForm):
    required_css_class = 'required'
    class Meta:
        model = Venue
        fields = '__all__'
 
class ConcertForm(forms.ModelForm):
    required_css_class = 'required'
    class Meta:
        model = Concert
        fields = '__all__'
 
class GenreForm(forms.ModelForm):
    required_css_class = 'required'
    class Meta:
        model = Genre
        fields = '__all__'
 
class ArtistManagementForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = ArtistManagement
        fields = '__all__'
 
class VolunteerApplicationForm(forms.ModelForm):
    required_css_class = 'required'
    status = forms.ModelChoiceField(
        queryset=ApplicationStatus.objects.all(),
    )
    
    class Meta:
        model = VolunteerApplication
        fields = ['user', 'area', 'status', 'user_comment', 'org_comment', 'year']
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(VolunteerApplicationForm, self).__init__(*args, **kwargs)
        self.fields['user'].label = "Ansøger"
        self.fields['year'].label = "År"
        self.fields['area'].label = "Frivilligområde"
        self.fields['user_comment'].label = "Kommentar"
        self.fields['status'].label = "Status"
        self.fields['org_comment'].label = "Arrangørnoter"
        
        # Filter areas to the current year and groups they are member of and exclude 'arrangør'
        current_year = Year.current_year()
        group_names = self.user.groups.values_list('name', flat=True) if self.user else []
        
        # Remove fields if the user does not have the permission
        if self.user and not self.user.has_perm('year.view_orgcomment'):
            del self.fields['org_comment']
        if self.user and not self.user.has_perm('year.view_volunteerapplicationstatus'):
            del self.fields['status']
        if self.user and not self.user.has_perm('accounts.view_allusers'):
            del self.fields['user']

        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name in ['year','user']:
                field.disabled = True
            if field_name in ['area'] and not self.user.has_perm('year.change_volunteerapplicationarea'):
                field.disabled = True
            field.widget.attrs['class'] = 'form-control'

class VolunteerApplicationCreateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = VolunteerApplication
        fields = ['user', 'area', 'status', 'user_comment', 'org_comment', 'year']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.area = kwargs.pop('area', None)  # Get the preselected area from kwargs
        super(VolunteerApplicationCreateForm, self).__init__(*args, **kwargs)
        # Filter areas to the current year and groups they are a member of and exclude 'Arrangør'
        current_year = Year.current_year()

        area_queryset = VolunteerArea.objects.filter(
            year=current_year,
            published=True,
            volunteerCount__lt=F('volunteerMax')  # Only areas with space
        ).exclude(name='Arrangør')

        # Define the 'area' field as a ModelChoiceField
        self.fields['area'] = forms.ModelChoiceField(
            queryset=area_queryset,
            label="Frivilligområder"
        )

        # Set initial value for 'area' field if self.area is provided
        if self.area:
            self.fields['area'].initial = VolunteerArea.objects.filter(id=self.area).first()
        
        approved_applications = VolunteerApplication.objects.filter(
                status=2,
                year=current_year
                )
        self.fields['user'].queryset = User.objects.exclude(
                volunteer_application__in=approved_applications
                )

        # Other field setups remain the same
        self.initial['year'] = current_year
        self.fields['year'] = forms.ModelChoiceField(
            queryset=Year.objects.all(),
            initial=current_year,
            widget=forms.HiddenInput()
        )
        
        initial_status = ApplicationStatus.objects.get(pk=1)
        self.initial['status'] = initial_status
        self.fields['status'] = forms.ModelChoiceField(
            queryset=ApplicationStatus.objects.all(),
            initial=initial_status
        )

        #current_time = timezone.now()
        #self.initial['time'] = current_time
        #self.fields['time'] = forms.DateTimeField(initial=current_time, widget=forms.HiddenInput())

        # Set field labels and permissions
        self.fields['user'].label = "Ansøger"
        self.fields['year'].label = "År"
        self.fields['area'].label = "Frivilligområde"
        self.fields['user_comment'].label = "Kommentar"
        self.fields['status'].label = "Status"
        self.fields['org_comment'].label = "Arrangørnoter"

        if self.user and not self.user.has_perm('year.view_volunteerapplicationstatus'):
            del self.fields['status']
        else:
            approved_status = ApplicationStatus.objects.get(pk=2)
            self.initial['status'] = approved_status
        if self.user and not self.user.has_perm('year.view_orgcomment'):
            del self.fields['org_comment']
        if self.user and not self.user.has_perm('accounts.view_allusers'):
            del self.fields['user']

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput':
                field.widget.attrs['class'] = 'form-control'
    
    def save(self, commit=True):
        # Override the save method to set the year and status values before saving
        instance = super(VolunteerApplicationCreateForm, self).save(commit=False)
        if instance.year is None:
            instance.year = Year.current_year()
        if instance.status is None:
            instance.status = ApplicationStatus.objects.get(pk=1)
        if commit:
            instance.save()
        return instance

class VolunteerAreaForm(forms.ModelForm):
    required_css_class = 'required'
    volunteerCount = forms.IntegerField(
            disabled=True
            )
    managerCount = forms.IntegerField(
            disabled=True
            )
    start_time = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        input_formats=['%Y-%m-%dT%H:%M']
    )
    end_time = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        input_formats=['%Y-%m-%dT%H:%M']
    )

    class Meta:
        model = VolunteerArea
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(VolunteerAreaForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Navn"
        self.fields['published'].label = "Udgivet"
        self.fields['mail'].label = "Email"
        self.fields['volunteerCount'].label = "Tilmeldte frivillige"
        self.fields['volunteerMax'].label = "Frivilligpladser"
        self.fields['managerCount'].label = "Tilmeldte afviklere"
        self.fields['managerMax'].label = "Afviklerpladser"
        self.fields['start_time'].label = "Starttid"
        self.fields['end_time'].label = "Sluttid"
        self.fields['bg'].label = "Billede"
        self.fields['short_description'].label = "Kort beskrivelse"
        self.fields['long_description'].label = "Lang beskrivelse"
        
        # Convert start_time and end_time to local time (Danish time or user local time)
        if self.instance and self.instance.pk:
            self.fields['start_time'].initial = localtime(self.instance.start_time).strftime('%Y-%m-%dT%H:%M')
            self.fields['end_time'].initial = localtime(self.instance.end_time).strftime('%Y-%m-%dT%H:%M')
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class VolunteerAreaCreateForm(forms.ModelForm):
    required_css_class = 'required'
    start_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    end_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    
    class Meta:
        model = VolunteerArea
        fields = (
                'year', 
                'name', 
                'published',
                'mail',
                'volunteerMax',
                'managerMax',
                'start_time',
                'end_time',
                'bg',
                'short_description',
                'long_description')
        
    def __init__(self, *args, **kwargs):
        self.volunteerarea = kwargs.pop('volunteerarea', None)
        super(VolunteerAreaCreateForm, self).__init__(*args, **kwargs)
        
        # Set labels for visible fields
        self.fields['year'].label = "År"
        self.fields['name'].label = "Name"
        self.fields['published'].label = "Udgivet"
        self.fields['mail'].label = "Mail"
        self.fields['volunteerMax'].label = "Frivilligpladser"
        self.fields['managerMax'].label = "Afviklerpladser"
        self.fields['start_time'].label = "Start"
        self.fields['end_time'].label = "Slut"
        self.fields['bg'].label = "Billede"
        self.fields['short_description'].label = "Kort beskrivelse"
        self.fields['long_description'].label = "Lang beskrivelse"
        
        # Set initial values
        cur_year = Year.current_year() # Get current year
        self.initial['year'] = cur_year 

        # set initial values to the ones from last year
        july_first = date(cur_year.year, 7, 1) # Start from July 1st of the given year
        first_saturday = july_first + timedelta(days=(5 - july_first.weekday()) % 7) # Find the first Saturday in July
        fourth_saturday = first_saturday + timedelta(weeks=3) # The fourth Saturday is 3 weeks (21 days) after the first Saturday

        self.initial['start_time'] = fourth_saturday
        self.initial['end_time'] = fourth_saturday 
       
        if self.volunteerarea:
            last_year = VolunteerArea.objects.filter(name=self.volunteerarea.name).order_by('-year').first() # Get last year's volunteer area
            self.initial['name'] = last_year.name
            self.initial['mail'] = last_year.mail
            self.initial['volunteerMax'] = last_year.volunteerMax
            self.initial['managerMax'] = last_year.managerMax
            self.initial['bg'] = last_year.bg
            self.initial['short_description'] = last_year.short_description
            self.initial['long_description'] = last_year.long_description

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput' and field_name != 'published':
                field.widget.attrs['class'] = 'form-control'

class VolunteerShiftEditForm(forms.ModelForm):
    required_css_class = 'required'
    start_time = forms.DateTimeField(
        widget=forms.DateTimeInput(
        attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        input_formats=['%Y-%m-%dT%H:%M']
    )
    end_time = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        input_formats=['%Y-%m-%dT%H:%M']
    )
    note = forms.CharField(
            widget=forms.Textarea, 
            required=False,
            label="Noter", 
            )

    class Meta:
        model = VolunteerShift
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.cur_user = kwargs.pop('cur_user', None)
        self.area = kwargs.pop('area', None)
        super(VolunteerShiftEditForm, self).__init__(*args, **kwargs)
        self.fields['start_time'].label = "Starttid"
        self.fields['end_time'].label = "Sluttid"
        self.fields['user'].label = "Bruger"
        self.fields['area'].label = "Område"
        self.fields['note'].label = "Noter"
       
        # restrict the queryset for volunteer areas, that the user is a member of
        current_year = Year.current_year()
        group_names = self.cur_user.groups.values_list('name', flat=True)
        member_areas = VolunteerArea.objects.filter(
                name__in=group_names,
                year=current_year
                )
        self.fields['area'].queryset = member_areas
        
        if self.area:
            approved_applications = VolunteerApplication.objects.filter(
                status_id=2,
                area=self.area
            )
            volunteers = User.objects.filter(volunteer_application__in=approved_applications).distinct()
            self.fields['user'].queryset = volunteers
        else:
            self.fields['user'].queryset = User.objects.filter(username=None)

        # Convert start_time and end_time to local time (Danish time or user local time)
        if self.instance and self.instance.pk:
            self.fields['start_time'].initial = localtime(self.instance.start_time).strftime('%Y-%m-%dT%H:%M')
            self.fields['end_time'].initial = localtime(self.instance.end_time).strftime('%Y-%m-%dT%H:%M')
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class VolunteerShiftCreateForm(forms.ModelForm):
    required_css_class = 'required'
    start_time = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        input_formats=['%Y-%m-%dT%H:%M']
    )
    end_time = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        input_formats=['%Y-%m-%dT%H:%M']
    )
    note = forms.CharField(
            widget=forms.Textarea, 
            required=False,
            label="Noter", 
            )
    
    class Meta:
        model = VolunteerShift
        fields = '__all__'
        
    def __init__(self, *args, **kwargs):
        self.volunteerarea = kwargs.pop('volunteerarea', None)
        self.user = kwargs.pop('user', None)
        super(VolunteerShiftCreateForm, self).__init__(*args, **kwargs)
        
        # Set labels for visible fields
        self.fields['start_time'].label = "Starttid"
        self.fields['end_time'].label = "Sluttid"
        self.fields['user'].label = "Bruger"
        self.fields['area'].label = "Område"
        self.fields['note'].label = "Noter"
       
        # restrict the queryset for volunteer areas, that the user is a member of
        current_year = Year.current_year()
        group_names = self.user.groups.values_list('name', flat=True)
        member_areas = VolunteerArea.objects.filter(
                name__in=group_names,
                year=current_year
                )
        self.fields['area'].queryset = member_areas
        self.fields['area'].widget.attrs.update({'id': 'volunteerarea-select'})
        
        # Set initial values
        if self.volunteerarea:
            self.volunteerarea_instance = VolunteerArea.objects.get(id=self.volunteerarea)
            if self.volunteerarea_instance:
                self.initial['area'] = self.volunteerarea_instance
                print(f"initial area: {self.initial['area']}")
                area_start = self.volunteerarea_instance.start_time
                latest_shift = VolunteerShift.objects.filter(area=self.volunteerarea_instance).order_by('-start_time').first()
                if latest_shift:
                    self.initial['start_time'] = latest_shift.end_time
                    self.initial['end_time'] = latest_shift.end_time + timedelta(hours=6) 
            # Only display users with approved applications 
            approved_applications = VolunteerApplication.objects.filter(
                status_id=2,
                area=self.volunteerarea
            )
            volunteers = User.objects.filter(
                    volunteer_application__in=approved_applications
                    ).distinct()
            self.fields['user'].queryset = volunteers
        else:
            self.fields['user'].queryset = User.objects.filter(username=None)
       
        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput' and field_name != 'published':
                field.widget.attrs['class'] = 'form-control'
