from django.test import TestCase
from year.models import (Year, 
                        Page, 
                        ArtistApplication,
                        ApplicationStatus,
                        VolunteerApplication, 
                        Donation, 
                        Event, 
                        Artist, 
                        Program, 
                        VolunteerArea)
from .forms import (YearForm,
                    YearCreateForm,
                    PageForm, 
                    ArtistApplicationForm,
                    ArtistApplicationCreateForm,
                    VolunteerApplicationForm, 
                    VolunteerApplicationCreateForm, 
                    DonationForm, 
                    DonationCreateForm,
                    EventForm, 
                    EventCreateForm, 
                    ArtistForm,
                    ArtistCreateForm,
                    ProgramForm,
                    ProgramCreateForm,
                    VolunteerAreaForm,
                    VolunteerAreaCreateForm)


class YearTestCase(TestCase):
    def read(self):
        year = Year.objects.get(pk=1).year
        published = Year.objects.get(pk=1).published
        start_time = Year.objects.get(pk=1).start_time
        end_time = Year.objects.get(pk=1).end_time
        banner_image = Year.objects.get(pk=1).banner_image
