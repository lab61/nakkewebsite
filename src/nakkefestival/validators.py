from django.core.exceptions import ValidationError
import re

class CustomPasswordValidator:
    def validate(self, password, user=None):
        if len(password) < 10:
            raise ValidationError("Kodeordet skal være minimum 10 karakterer langt.")
        if not re.search(r'\d', password):
            raise ValidationError("Kodeordet skal indeholde minimum 1 tal.")
        if not re.search(r'[!@#$%^&*(),.?":{}|<>]', password):
            raise ValidationError("Kodeordet skal indeholde minimum 1 specialtegn [!@#$%^&*(),.?:{}|<>].")

    def get_help_text(self):
        return "Dit kodeord skal indeholde være minimum 10 karakterer langt, og indeholde tal og specialtegn."
