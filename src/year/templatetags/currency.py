from django import template

register = template.Library()

@register.filter(name='danish_currency')
def danish_currency(value):
    """
    Format the value as Danish currency (kr), with comma as decimal separator
    and dot as thousand separator.
    """
    try:
        value = float(value)
    except (ValueError, TypeError):
        return value  # Return the original value if conversion fails

    # Format the currency with the Danish convention:
    # - Thousand separator: dot (.)
    # - Decimal separator: comma (,)
    # - Currency symbol: kr
    formatted_value = f"{value:,.2f}".replace(",", "X").replace(".", ",").replace("X", ".")
    return f"{formatted_value} kr"

