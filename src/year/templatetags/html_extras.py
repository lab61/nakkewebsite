from django import template
from django.utils.safestring import mark_safe
from bleach import clean, ALLOWED_TAGS, ALLOWED_ATTRIBUTES

register = template.Library()

@register.filter(name='filter')
def markdown_format(html):
    # Convert ALLOWED_TAGS from frozenset to list and extend with custom tags
    allowed_tags = list(ALLOWED_TAGS) + ['p', 'pre', 'code', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'ul', 'ol', 'li', 'strong', 'em', 'a', 'blockquote', 'hr', 'table', 'thead', 'tbody', 'tr', 'th', 'td', 'i', 'br', 'dl', 'dd', 'dt']
    
    allowed_attributes = {
        **ALLOWED_ATTRIBUTES, 
        'a': ['href', 'title', 'rel', 'name'],
        'img': ['src', 'alt', 'title', 'width', 'height'],
        'i': ['class'],  # Allowing 'class' attribute on 'i' tags for icons
        '*': ['class', 'id'],  # Allowing class and id attributes on all elements
    }
    
    # Clean the HTML
    safe_html = clean(html, tags=allowed_tags, attributes=allowed_attributes, strip=True)
    # Mark the result as safe HTML to be rendered in the template
    return mark_safe(safe_html)

