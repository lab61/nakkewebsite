from django import forms
from django.utils import timezone
from datetime import timedelta, date
from year.models import Year, Page, Donation, Event, Artist, Program, ApplicationStatus, ArtistApplication, Venue, Concert, Genre, ArtistManagement, VolunteerApplication, VolunteerArea, VolunteerShift
from accounts.models import User, Mail
from django.contrib.postgres.forms import SimpleArrayField
from django.utils.timezone import localtime
from django.core.exceptions import ValidationError
from django.db.models import F
from django.shortcuts import redirect, get_object_or_404 
from ckeditor.widgets import CKEditorWidget
 
class SendApplicationForm(forms.Form):
    required_css_class = 'required'
    
    contact = forms.CharField(
            required=True,
            label="Kontaktpersons navn"
            )
    email = forms.EmailField(
            required=True,
            label="Email"
            )
    phone = forms.CharField(
            required=True,
            label="Telefon"
            )
    alt_contact = forms.CharField(
            required=False,
            label="Alternativ kontaktmulighed"
            )
    name = forms.CharField(
            required=True,
            label="Kunstnernavn"
            )
    description = forms.CharField(
            widget=CKEditorWidget(), 
            required=True,
            label="Beskrivelse", 
            )
    image = forms.ImageField(
            required=False,
            label="Billede",
            )
    link = forms.URLField(
            required=False,
            label="Link"
            )
    file = forms.FileField(
            required=False,
            label="Fil"
            )
    comment = forms.CharField(
            required=False,
            label="Kommentarer",
            widget=forms.Textarea,
            )
    
    def __init__(self, *args, **kwargs):
        super(SendApplicationForm, self).__init__(*args, **kwargs)

        # Apply the 'form-control' class to all visible fields
        for field_name, field in self.fields.items():
            if field.widget.__class__.__name__ != 'HiddenInput':
                field.widget.attrs['class'] = 'form-control'
    
    def clean(self):
        if not self.cleaned_data['link'] and not self.cleaned_data['file']:
            raise ValidationError("Du skal tilføje musik som link eller fil.")

