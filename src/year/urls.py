from django.urls import path

from django.conf.urls import handler403
from . import views

app_name = "year"
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('information/praktisk_information', views.Praktisk_informationView.as_view(), name='praktisk_information'),
    path('information/bliv_frivillig', views.Bliv_frivilligView.as_view(), name='bliv_frivillig'),
    path('information/bliv_kunstner', views.Bliv_kunstnerView.as_view(), name='bliv_kunstner'),
    path('information/fallesbus_til_nakke', views.Fallesbus_til_nakkeView.as_view(), name='fællesbus_til_nakke'),
    path('om/om_nakkefestival', views.Om_nakkefestivalView.as_view(), name='om_nakkefestival'),
    path('om/historien', views.HistorienView.as_view(), name='historien'),
    path('om/find_vej', views.Find_vejView.as_view(), name='find_vej'),
    path('om/billeder', views.BillederView.as_view(), name='billeder'),
    path('om/donationer', views.DonationerView.as_view(), name='donationer'),
    path('om/vedtagter', views.VedtagterView.as_view(), name='vedtagter'),
    path('om/presse', views.PresseView.as_view(), name='presse'),
    path('om/kontakt', views.KontaktView.as_view(), name='kontakt'),
    path('musik/kunstnere/<int:year>/', views.KunstnereView.as_view(), name='kunstnere'),
    # path('musik/kunstnere/<int=year>', views., name=''),
    # path('musik/kunstnere/<slug=band>', view., name''),
    path('musik/program/<int:year>/', views.ProgramView.as_view(), name='program'),
    # path('<int:year>?/musik/program', views.ProgramView.as_view(), name='program'),
    path('musik/events/<int:year>/', views.EventsView.as_view(), name='events'),
    path('kunstner_tilmelding/', views.SendApplicationView.as_view(), name='send_application'),
    path('om/privatlivspolitik/', views.PrivacyView.as_view(), name='privacy'),
] 


handler403 = views.CustomPermissionDeniedView.as_view()
