from django.contrib import admin

from .models import (
  Page,
  Year,
  Donation,
  Program,
  Event,
  Artist,
  ArtistApplication,
  Venue,
  Concert,
  Genre,
  ArtistManagement,
  VolunteerApplication,
  VolunteerArea,
  TicketType,
  Ticket,
  Stage,
  VolunteerShift
  )

# Register your models here.
models = [Page,
          Year,
          Donation,
          Program,
          Event,
          Artist,
          ArtistApplication,
          Venue,
          Concert,
          Genre,
          ArtistManagement,
          VolunteerApplication,
          VolunteerArea,
          TicketType,
          Stage,
          Ticket,
          VolunteerShift]

for model in models:
    admin.site.register(model)
