# Temp file for global template variable, while working in dev env
# TODO
# Remove this file in prod

from django.utils import timezone
from datetime import timedelta, datetime
from .models import Year, VolunteerArea
import os
from django.utils import timezone

def current_year(request):
    # Get the current year
    offset_date=timezone.now()+timedelta(days=120)
    offset_year=offset_date.year

    # Get or create the Year object
    year_obj, created = Year.objects.get_or_create(year=offset_year)

    return {'current_year': year_obj.year}
    # cur_year = Year.objects.get_or_create(year=timezone.now().year)[0] - timedelta(days=90)
    # return {'cur_year': cur_year}

def global_context(request):
    website_link = os.getenv('WEBSITE_LINK')
    assert website_link is not None, "Websitelink is None"
    
    link = '/static/'

    return {
            'STATIC_DEV': link,
    }

def default_area(request):
    if not request.user.is_authenticated:
        return {'default_area': None}  # Return None for unauthenticated users
    
    group_names = request.user.groups.values_list('name', flat=True)
    current_year = Year.current_year()
    area = VolunteerArea.objects.filter(
        year=current_year,
        name__in=group_names
    ).first()
    print(f"default_area: {area.id if area else 'None'}")   
    # Return the area ID or None if no area is found
    return {'default_area': area.id if area else None}
