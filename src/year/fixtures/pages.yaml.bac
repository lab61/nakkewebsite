- model: year.page
  pk: 1
  fields:
    year: 1
    title: "Praktisk Information"
    bg_image: 'info/praktisk-info.jpg'
    short_description: 'Modsat andre større festivaler er der nogle genstande vi desværre ikke kan tillade på vores lille søde festivalplads.'
    long_description: '
Derfor, ingen:

- Pavilloner
- Store lydanlæg (med og uden hjul) / Dette gælder også store ghettoblastere eller lignende mobile anlæg
- Alt af glas er strengt forbudt på festivalpladsen – fra alkohol, øl, syltetøj osv. Skal du have noget ind, der er købt i en glasbeholder, må du hælde det over på noget andet først.
- Ingen vogne eller trækvogne; de må parkeres pænt ude foran pladsen. (Det samme gælder også cykler)
- Det er god stil at have sit eget bestik og tallerken med når man spiser i det veganske køkken, da vi så sparer en masse kedelig skrald (der vil være opvask stationer så du kan spise af en ren tallerken).

### Telte

Nakke er en lille festival med plads til små telte, derfor anbefaler vi alle vores gæster kun at have den størrelse telt, som man har brug for med. Der er ikke en kultur for camps eller for at hænge ud i teltområdet, men derimod foran scenerne, i baren eller i Kreas chillout områder. Slå gerne dit telt op så tæt på hinanden, så vi slipper for at flytte rundt på telte, når de er sat op. Det er vigtigt at teltene bliver sat op inden for det markerede område af hensyn til bare tæer og brandsikkerhed.

Kort sagt, vær praktisk, effektiv, hensynsfuld og fornuftig når det gælder teltopsætning og bagage. <3 <3

### Åben ild:

Kun tilladt på vores kogeøer, som findes på forpladsen.

### Endagsbillet:

En endagsbillet gælder fra kl.12:00 til 12:00 næste dag.

Det er ikke tilladt at have sit eget telt med på pladsen med en endagsbillet, dog må du gerne overnatte i en kammerats, eller ny vens telt <3

### Køb din billet, Online/ i døren:

Du kan til og med onsdag købe din billet online, men fra onsdag kl 12.00 kan du kun købe i indgangen hvis altså der er flere biletter tilbage.

### Donation af årets overskud

I år modtager I en stemmebrik, når i får jeres armbånd, og med den kan I læse og stemme om hvad årets overskud skal gå til.

### Narkopolitik

Nakkefestivalen støtter ikke indtagelse, anvendelse eller salg af euforiserende stoffer på festival- og campingområdet.

### Alder

Personer under 15 år skal være i følgeskab med en voksen.

For at være frivillig på festivalen skal man være minimum 15 år (visse frivillige poster skal man være ældre).

### Husdyr
Husdyr er tilladt på pladsen fra 10-22.
Husdyr er velkomne på festivalen, men de skal være i snor og ejeren skal sørge for at samle afføring op - og selvfølgelig sørge for, at dyret har det godt med skygge, vand og mad.
Oplevelse af hund på festival af Miki Westh:
"Jeg har haft hund med og jeg vil på det kraftigste fraråde det hvis det er flere dage du har tænkt dig at have den med. Min er en 40 kg Christiania hund som er meget social og let at omgås.
Men han fik det simpelthen dårligt over alt den larm, mennesker og kaos. Så til sidst gik har klods op ad mig og rystede.
Udover det så er der nogle få på Nakke som har en helt ekstrem angst over for alle hunde. Jeg vil stærkt anbefale dig at få din hund passet.
Glædelig Nakke"
Når alt dette er sagt er der kun tilbage at sige:
Vi glæder os sindssygt meget til at feste med jer fra onsdag, og vi ved allerede nu, at det bliver en fest uden lige! <3 <3 <3
    '

- model: year.page
  pk: 2
  fields:
    year: 1
    title: Bliv Frivillig
    bg_image: info/frivillig.jpg
    short_description: ''
    long_description: '
### Til medhjælpere
Nakkefestivalen er ikke skabt af store virksomheder eller politiske bevægelser, men af helt almindelige mennesker som dig og mig. Så kom derfor glad og vær med til at skabe festen sammen med os andre!
Som medhjælper får du et festivalarmbånd for din indsats. Vi forventer at du arbejder 16-24 timer, hvis du arbejder under festivalen, og mere end 24 timer, hvis du arbejder før eller efter festivalen. Arbejder du før eller efter festivalen vil der så vidt muligt blive sørget for mad, da det ellers kan tage tid at finde udenfor festivalen. Arbejder du ekstra fantastisk hårdt og længe, så kan din arrangør give dig en MØL-billet pr. time du arbejder over aftalt tid.
Hvis du har lyst til at give en ekstra hånd på festivalen som afvikler for et område, så skriv det i din frivillig tilmelding.
Har du en spirende arrangør gemt i maven? så skriv en mail til [formand@nakkefestival.dk](mailto:formand@nakkefestival.dk)

Man skal minimum være 15 år for at være medarbejder på Nakkefestival
- Områder med grøn knap har åbent for tilmelding.
- Områder med gul knap har endnu ikke åbnet deres tilmelding.
- Områder med rød knap har lukket for tilmelding.
### Sikkerhedsmøde:
Det er obligatorisk for alle medarbejdere på Nakkefestival at deltage i sikkerhedsmødet **tirsdag 23/07/2024 klokken 19:00**. Efter mødet holder vi frivillig fest for alle medarbejdere, afviklere og arrangører. 
    '

- model: year.page
  pk: 3
  fields:
    year: 1
    title: Information til kunstnere
    bg_image: info/kunstnere.jpg
    short_description: ''
    long_description: '
## Vil du optræde på Nakkefestival – så skriv til os i Booking!
Nakkefestival er i over 25 år blevet skabt i fællesskab af musikere, kreative personer og ildsjæle. Vi ønsker alle at skabe et frirum fuld af kærlighed og fællesskab, hvor ingen tjener penge på andre. Nakkefestival er derfor 100 procent not-for profit og arrangeret af frivillige arrangører. Vi har ingen sponsoraftaler – og al festivalens overskud doneres væk.
Dette betyder også, at vi ikke tilbyder tarif eller honorar for din optræden, men vi giver naturligvis transportgodtgørelse, forplejning, og gratis adgang til hele festivalen. Vi håber, at du har lyst til at deltage på de vilkår, og skabe denne ganske særlige festival sammen med os.
Alle kunstneriske udtryk er velkomne på Nakkefestival. Vi lægger dog især stor vægt på originalitet og alternative udtryk, og Nakkefestival byder derfor på en alternativ atmosfære med sære eksistenser og utraditionelle optrædende.
### Ansøgninger:
Hvis du/I ønsker at optræde på Nakkefestival, kan du/I sende en mail til os på: [booking@nakkefestival.dk](mailto:booking@nakkefestival.dk)
### Nakkefestival søger altid optrædende!
Vi opererer ikke med en booking-deadline men booker hele året. Men jo før I sender os en mail jo bedre. Vi får rigtigt mange mails hvert år, og svarer derfor kun de bands, vi vælger at booke. Vi kan dog garantere, at alle der sender en ansøgning, vil blive vurderet.
Hvis ikke I får svar i år, så prøv meget gerne igen næste år.
Vi gør også opmærksom på at Nakkefestival ikke booker cover/kopi-bands, og at ansøgninger fra sådanne vil blive frasorteret med det samme.
Vi ser frem til at modtage din/jeres ansøgning.
Kys i nakken,
Booking-gruppen <3
    '

- model: year.page
  pk: 4
  fields:
    year: 1
    title: Bus til og fra Nakkefestival
    bg_image: info/bustur.jpg
    short_description: 'Fællesbussen til Nakke er den hyggeligste og grønneste måde at få dig og dine venner med på Nakkefestival! I samarbejde med Solibus arrangerer vi bustransport til og fra festivalen. Der er begrænsede pladser, så skynd dig at få fat i en billet!'
    long_description: '
Turen tager omkring en time og koster 135 kroner (Kbh) - det er billigere end offentlig transport og bedre for miljøet end at køre selv. Det er oven i købet en fantastisk undskyldning for at tilbringe endnu mere tid sammen med dine søde og dejlige Nakkevenner! <3
Busserne til festivalen kører fra Stengade nr. 18 i Kbh. Du skal være ved bussen en halv time før afgang, så vi får alle tjekket ind og din bagage med.
<!--  Billetoversigten skal slettes, hvis vi laver knapper istedet -->
### Vi arrangerer følgende ture:
FÆLLESBUS TIRSDAG DEN 23/7 KL. 15
FOR FRIVILLIGE (Kbh - Nakke)
FÆLLESBUS ONSDAG DEN 24/7 KL. 12
FOR GÆSTER (Kbh - Nakke)
FÆLLESBUS SØNDAG DEN 27/7 KL. 12
FOR ALLE (Nakke - Kbh)
### Praktisk information:
Pak højst en taske og et telt per person, der er begrænset plads til baggage. Husk at Nakke elsker små telte og fællesstemning - ikke store telte og camps!
Husk din billet - både til bussen og festivalen! Der vil være én, der tjekker din billet, når du stiger ombord på bussen
Billetterne til bussen sælges først til mølle, sidste år var vi hurtigt udsolgt, så skynd dig at køb en billet :)
    '

- model: year.page
  pk: 5
  fields:
    year: 1
    title: Om Nakkefestival
    bg_image: om/om-nakke.png
    short_description: ''
    long_description: '
Når man træder ind på Nakkefestivalen, så føler man sig velkommen. Vi er en stor lejr, hvor vi har fællestelte som tipien og plads til at hænge ud rundt omkring scenerne. Her er ikke tradition for små private lejre, fordi folk, som kommer på Nakke, er åbne for nye mennesker og fordi det netop er charmen ved Nakke. Rummet er fyldt med kreative indslag, som en invitation til smil og glæde, og næsten uden undtagelser kommer en stor del fra festivaldeltagerne selv.
På Nakke kommer man ikke på festival, man er festival!
Nakkefestival er en musikfestival, og på den front er festivalen også inkluderende. Der bookes med plads til mange forskellige genrer, og helt essentielt bookes der gerne nye udfordrende kunstnere, som ellers ikke falder under normale kategorier. Med to scener fyldes rummet med blandinger af alt fra historiefortællinger til at stå op på, henover kunstnere der synger på selvopfundne sprog, til up-coming bands med god lyd og slutter af med fede beats og dans på vores lukkede scene.
På Nakke favner vi og udvider.
Og barskabet kan folk godt lade stå derhjemme. Man må ikke tage glas med ind på festivalen, men det er blot fordi, at fodboldbanerne er lånt, og de skal være helt rene for alt, når de afleveres igen. Men der sørges for, at man ikke vil mangle noget til ganen, hverken fast eller flydende. Uanset om man er kødspiser eller veganer, så er der mad til så lave priser, og uanset om man er til øl, bitter eller drinks, så findes det, man mangler til festen.
Nakke er et lille sted, hvor man ikke skal langt for den næste gode oplevelse.
Nakkefestivalen er et åndehul, hvor du er velkommen til at slappe af i, blive udfordret i, blive nurset i og skabe den gode fest i.

Nakkeånden glæder sig til at se dig  <3 <3 <3
    '

- model: year.page
  pk: 6
  fields:
    year: 1
    title: Historien
    bg_image: om/historien.jpg
    short_description: 'Her er et lille eventyr til dem der undre sig over hvordan den første Nakkefestival blev sat i verden'
    long_description: '
#### Kasper Andersen fortæller:
Det er en smuk maj aften i 2017 og jeg sidder og nyder en kop kaffe til lyden af fuglekvidder og den dér, med ham dér, der synger så pænt, da stilheden og idyllen med ét bliver brutalt slagtet på barndommens evindelige uvidenhedsalter: ”Far, far! Må jeg ikke bygge en 7 meter høj atomkylling i baghaven og kalde den Bjarne C?!” Det er min knægt Asbjørn der spørger.
Mine tanker flakker og minderne fra 1996 dukker pludselig op fra hjernebakkens yderste-indre: Jonathan og jeg havde netop set den legendariske Woodstock-video og vi var helt oppe at køre, sådan teenager-oppe-at-køre. Vi ville sgu da også lave sådan en festival på en mark i Nakke eller Rørvig. Det krævede jo bare noget strøm og et par lægter; så var vi i gang! Som dagen skred frem gik det dog op for os at vi blot var 15 og 16 år og hvordan hulen skulle vi bære os ad med det? Hvordan skulle vi købe øllet? En automobil var nok også nødvendig… ideen blev skrinlagt, men i løbet af de næste dage snakkede vi videre om det; vi kunne simpelthen ikke lade være.
Der foregik intet i Rørvig og Nakke i 1996 der kunne være fedt for et par hippie-børn som os. Hvis man ikke elskede Cotton-Eyed Joe og lerdueskydning, var man ikke rigtig velkommen i dét samfund, og det ville vi gerne ændre på.
Til sidst begyndte far Olox at drille os fordi vi havde sådan et tudefjæs på over opgaven. En opgave vi, uden selv at være helt bevidste om, jo allerede var gået i gang med. Det blev for meget for de to teenagere så de sked hul i problemerne og tog kontakt til folk fra nær og fjern.
Vikar-musiklæren lovede at vi sagtens kunne låne trommer og baggear af skolen og en fra vores klasse kendte en murer der havde en presenning (prinshenning på Nakkejansk) og så skrev vi et brev til borgermesteren om vi lige måtte låne fodboldbanen ved Rørvig Friskole,
fordi vi for øvrigt lige ville lave en festival? Af uransagelige årsager syntes denne (læs: borgermesteren) at det var en glimrende idé og så var det på med handsken.
Penge havde vi selvsagt ingen af så vi måtte gøre noget så umoderne og grænseoverskridende som at tale med mennesker og fortælle dem om vores idé om en upolitisk nonprofit-festival, der for øvrigt skulle opstå med vores egen fantasi som eneste byggemateriale. Det tog en rum tid at få nogen med på dén idé (hvorfor vi også nogle år senere oprettede en gruppe der kunne tage sig af den absurde opgave).
Scenen blev rejst, bygget af lægter og en tung(!) murerprenshenning – jeg vil tro at en vindstyrke på 1 sek/m ville have væltede scenen og smadret Nakkefestival før den overhovedet startede, men til alt held var det vindstille den weekend.
Skolens trommesæt og forstærkere var sat op og noget så eksotisk som et fadølsanlæg med to fustager var også i hus. Hanerne blev administreret med hård hånd af forældre, der gerne ville se hvad hulen de der teenagere havde gang i. Så vidt jeg husker var der 4-6 bands, hovedsageligt bestående af arrangørerne selv. Disse mennesker var i og for sig også publikum, med få undtagelser.
Det blev en bragende succes: Scenen væltede ikke, vi fik drukket al øllen, vi spillede Jimmy Hendrix (eller forsøgte på det, undskyld Jimmy) og punk-musik for fuld rulle og vi fik ikke tæsk af lokale bønder – altså i sandhed en bragende succes og denne uventede optursfed af en oplevelse skreg på en to’er.
Sådan gik det altså til at vi stadig den dag i dag kan trække i arbejds-hippie-tøjet og en gang om året fordrive den korrupte og kapitalistiske verden for, for en kort stund, at kunne sætte os i græsset og nyde lyden af fuglekvidder og den dér, med ham dér, der synger så pænt… ”Far, far! Må jeg altså godt bygge den der Atomkylling eller ej!?” Ordene runger hult da jeg hører mig selv svare: ”Ja min dreng, selvfølgelig må du det”
    '

- model: year.page
  pk: 7
  fields:
    year: 1
    title: Find vej
    bg_image: om/find-vej.jpg
    short_description: 'Nakkefestival er en lille dansk non-profit musikfestival beliggende i Odsherred i det nordvestlige Sjælland. Endskønt festivalen strengt taget ligger indenfor Rørvigs grænser, er den opkaldt efter den lille landsby Nakke, der ligger tættere på festivalpladsen end selve Rørvig by. Nakkefestival afholdes på boldbanerne bag Rørvighallen med Rørvig Sognegård og Rørvig Friskole som naboer.' 
    long_description: '
Nakkefestival er en lille dansk non-profit musikfestival beliggende i Odsherred i det nordvestlige Sjælland. Endskønt festivalen strengt taget ligger indenfor Rørvigs grænser, er den opkaldt efter den lille landsby Nakke, der ligger tættere på festivalpladsen end selve Rørvig by. Nakkefestival afholdes på boldbanerne bag Rørvighallen med Rørvig Sognegård og Rørvig Friskole som naboer.
<!-- Skal måske laves for sig selv -->
### Festivalens adresse under festivalen:
Nakkefestival
Søndervangsvej 43
4581 Rørvig

### Transport
Vil man tage brug af den offentlige transport på sin rejse mod Nakkefestival så er der fint med muligheder:
Kommer man fra København af, kan man tage regionaltoget mod Kalundborg, stå af på Holbæk station for derefter at tage bus 560 mod Nykøbing Sj. st. Fra Nykøbing St. st. kan man derfra tage bus 562 mod Nykøbing Sj. st., for så at stå af ved stoppet Sognegården (Odsherred). Der efter mangler man blot at gå et kort stykke vej til indgangen af Nakkefestivalen.
En anden mulighed er at tage S-toget, linje A til Hillerød st. skifte til Lokalbane 920R mod Hundested Havn St. og så tage færgen til Rørvig havn, hvorfra man enten kan gå, cykel, eller gå op og tage en bus 562 mod Nykøbing Sj. st og så endeligt stå af ved stoppestedet Sognegården (Odsherred).
Kommer man fra Roskilde af, eller længere vest på kan det igen bedst betale sig at tage til Holbæk station og derfra følge de ovenstående rejsebeskrivelser.
Vi glæder os til at se jer! <3 <3 <3
  '

- model: year.page
pk: 8
fields:
  year: 1
  title: Billeder fra tidligere år
  bg_image: om/billeder.jpg
  short_description: 'Her kan du finde billeder taget af Nakkefestivals frivillige fotografer.'
  long_description: '
Billederne er Nakkefestivals ejendom og må ikke deles eller anvendes uden skriftlig tilladelse fra Nakkefestivals arrangørhold. 
  '

- model: year.page
pk: 9
fields:
  year: 1
  title: Donationer
  bg_image: om/donationer.jpg
  short_description: 'Hvert år går overskuddet fra Nakkefestivalen til et velgørende formål.'
  long_description: '
## Overskuddet er tidligere gået til:
  '

- model: year.page
pk: 10
fields:
  year: 1
  title: Vedtægter og siddende bestyrelse
  bg_image: om/vedtaegter.jpg
  short_description: ''
  long_description: '
## Vedtægter for Foreningen Nakkefestival
### Kapitel 1: Navn og Formål
§1 Foreningens navn er Foreningen Nakkefestival.
§2 Foreningen hører hjemme i den kommune, hvor festivalen bliver afholdt.
§3 Foreningen Nakkefestivals formål er at støtte og udbrede dansk undergrundsmusik, samt være et mødested for forskellige kulturelle udtryk former. Dette imødekommes igennem afholdelse af den årlige Nakkefestival.
### Kapitel 2: Medlemmer
§4 Alle kan optages som medlem af Foreningen Nakkefestival.
Stk. 2 Medlemmer kaldes i daglig tale arrangører.
§5 Som medlem skal man være en del af virket i Foreningen Nakkefestival.
Stk. 2 Dette indebærer at komme til møder, og være ansvarlig for et selvvalgt område, der er accepteret af de andre arrangører.
§6 Kun medlemmer har stemmeret ved generalforsamlinger og kan blive valgt ind i foreningens bestyrelse.
§7 Alle medlemmer betaler et årligt kontingent. Kontingentet fastsættes af generalforsamlingen.
§8 Ingen medlemmer hæfter for foreningens aktiviteter.
Kapitel 3: Generalforsamlingen
§9 Generalforsamlingen er foreningens højeste myndighed.
§10 Generalforsamlingen afholdes medio februar.
Stk. 2 Indkaldelse udarbejdes af Foreningen Nakkesfestivals bestyrelse.
Stk. 3 Indkaldelse skal ske mindst 14 dage før, og varsles på relevante medier, som minimum festivalens intranet.
§11 Dagsordenen udsendes 7 dage før generalforsamling og skal mindst indeholde:
1. Valg af dirigent
2. Bestyrelsens årsberetning
3. Regnskabsaflæggelse til godkendelse
4. Forslag fra medlemmer
5. Fastsættelse af kontingent
6. Valg af bestyrelse:
  1) Valg af forperson
  2) Valg af næstforperson
  3) Valg af kasserer
  4) Valg af ordinære medlemmer
7. Eventuelt
§12 Forslag til generalforsamlingen skal være bestyrelsen i hænde senest 8 dage før generalforsamlingen.
§13 Forslag til vedtægtsændringer skal vedlægges dagsordenen til generalforsamlingen.
§14 Generalforsamlingen træffer beslutninger ved enighed, hvis muligt, ellers ved simpel flertalsafstemning.
§15 Ekstraordinær generalforsamling skal afholdes hvis bestyrelsen, eller 2/3 (to tredjedele) af medlemmerne udtrykker ønske herom. Begæringen skal indeholde en dagsorden.
§16 Ekstraordinær generalforsamling skal forløbe som en ordinær generalforsamling.
### Kapitel 4: Bestyrelsen
§17 Bestyrelsen består af forperson, næstforperson, kasserer og op til 5 ordinære medlemmer.
### Kapitel 5: Økonomi
§18 Foreningen Nakkefestivals regnskabsår følger kalenderåret.
§19 Regnskab føres af den i bestyrelsen valgte kasserer.
§20 Budget og eventuelle budgetoverskridelser skal vedtages af kassereren i forening med et andet bestyrelsesmedlem.
§21 Overskud efter den årlige Nakkefestival anvendes til velgørende formål, i overensstemmelse med gældende dansk lovgivning.
### Kapitel 6: Tegnings- & Hæftelsesret
§22 Foreningen tegnes af 2 af følgende i forening: forperson, næstforperson, kasserer.
§23 For foreningens forpligtelser hæfter alene dennes formue og ingen anden har noget personligt ansvar for foreningens forpligtelser.
### Kapitel 7: Eksklusion af foreningen Nakkefestival
§ 24 Ethvert medlem, der ikke opfylder kravene stillet i §5 stk 2 og/eller gør andre medlemmer utrygge, kan indstilles til eksklusion hos bestyrelsen. Det indstillede medlem skal orienteres øjeblikkeligt herom. Hvis halvdelen af bestyrelsen godkender indstillingen, skal den tages til anonym afstemning på det følgende arrangørmøde. Her skal 2/3 (to tredjedel) af de fremmødte arrangører godkende eksklusionen.
Stk. 2 Alle medlemmer har ret til at forsvare sig selv før afstemningen om eksklusion.
Stk. 3 En eksklusion kan i særlige tilfælde udvides komplet bortvisning fra festivalen, dog kun ved enstemmighed hos resten af medlemmerne. Personen kan kontakte bestyrelsen efter et år og kræve spørgsmålet om eksklusion genforhandlet til et arrangørmøde.
### Kapitel 8: Persondataerklæring
§ 24 Foreningen Nakkefestival behandler og opbevarer persondata digitalt for de frivillige og arrangører i overensstemmelse med gældende dansk lovgivning på området.
1. Dataen bruges til koordinere det frivillige festivalarbejde.
2. Nakkefestival indhenter samtykke til opbevaring af persondata, og dataen skal udleveres på forlangende af de frivillige.
3. Kun medlemmer af foreningen (arrangører) har adgang til dataen
### Kapitel 9: Vedtægtsændringer og opløsning
§25 Disse vedtægter kan kun ændres af generalforsamlingen. På generalforsamlingen skal der være 2/3 flertal blandt de afgivne stemmer for en vedtægtsændring. Blanke stemmer tæller ikke som afgivne stemmer.
Stk. 2: Ændringer træder i kraft umiddelbart efter generalforsamlingens afslutning.
§26 Foreningen Nakkefestival kan kun nedlægges på generalforsamling hvis 2/3 (to tredjedele) af de tilstedeværende stemmeberettigede stemmer for dette.
### Kapitel 10: Datering
- Vedtaget på stiftende generalforsamling 20/10 1999.
- Ændret ved ordinær generalforsamling 28/9 2003.
- Ændret ved ordinær generalforsamling 12/3 2016.
- Ændret ved ordinær generalforsamling 10/2/2018.
- Senest ændret ved ordinær generalforsamling 13/2/2021.
- Dirigentens underskrift: Andreas Jespersen 13/2/2021.
  '

- model: year.page
pk: 11
fields:
  year: 1
  title: Presse
  bg_image: om/presse.png
  short_description: ''
  long_description: '
### Kontakt os
[kommunikation@nakkefestival.dk](mailto:kommunikation@nakkefestival.dk)
### Brug af billeder
Brug frit af vores [Facebook albummer](https://www.facebook.com/pg/OfficielNakkefestival/photos/?tab=albums)
### Kunstnere og programmer
Se programmet fra [tidligere år](Kunstnere)
  '

- model: year.page
pk: 12
fields:
  year: 1
  title: Kontakt
  bg_image: om/kontakt.jpg
  short_description: ''
  long_description: '
### Er du musikker:
[booking@nakkefestival.dk](mailto:booking@nakkefestival.dk)
### Vil du være frivillig?

<!-- <i [class="fa-brands fa-facebook"></i>](https://www.facebook.com/pg/OfficielNakkefestival) -->
![Facebook logo][https://dev.nakkefestival.dk/static/img/svg/Facebook_logo.svg](https://www.facebook.com/pg/OfficielNakkefestival)
Skriv til os på [Facebook](https://www.facebook.com/pg/OfficielNakkefestival).
    '

- model: year.page
  pk: 13
  fields:
    year: 1
    title: Kunstnere
    bg_image: musik/kunstnere.jpg
    short_description: ''
    long_description: '

    '

- model: year.page
  pk: 14
  fields:
    year: 1
    title: Program
    bg_image: musik/program.jpg
    short_description: ''
    long_description: '
Vær opmærksom på at ændringer kan forekomme
    '

- model: year.page
  pk: 15
  fields:
    year: 1
    title: Events
    bg_image: musik/krea.jpg
    short_description: ''
    long_description: '

' 
