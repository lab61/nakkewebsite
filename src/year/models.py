from django.db import models
from accounts.models import User
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from ckeditor.fields import RichTextField
from datetime import datetime, time, timedelta
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

class Year(models.Model):
    year = models.IntegerField()
    published = models.BooleanField(default=False)
    start_time = models.DateTimeField(blank=True)
    end_time = models.DateTimeField(blank=True)
    banner_image = models.ImageField(upload_to='images/year/', blank=True)
    
    @classmethod
    def current_year(cls):
        # Set year object to the next year, if only 120 days from next year
        offset_date=timezone.now()+timedelta(days=120)
        offset_year=offset_date.year

        # return year object with offset year
        return cls.objects.get_or_create(year=offset_year)[0]

    def __str__(self):
        return str(self.year)

    class Meta:
        permissions = [
            ("view_hzd", "Can view Hzd"),
            ("access_wiki", "Can access Wiki"),
            ("access_cloud", "Can access Cloud drive"),
            ("access_chat", "Can access Chat"),
            ("access_mail", "Can access Mail"),
        ]

class Page(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE)
    
    title = models.CharField(max_length=50)
    bg_image = models.ImageField(upload_to='images/pages/', blank=True)
    short_description = models.CharField(max_length=500, blank=True)
    long_description = RichTextField(blank=True)
    organiser_page = models.BooleanField(default=False)
    
    def __str__(self):
        """
        How the record is represented in the django admin interface
        """
        return self.title

class Donation(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='donations')
    
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    receiver = models.CharField(max_length=50)
    published = models.BooleanField(default=False)
    phone = models.CharField(max_length=50, blank=True)
    mail = models.EmailField(blank=True)
    link = models.URLField(blank=True)

    def __str__(self):
        return f'{self.year} - {self.amount} kr.'
    
class Event(models.Model):
    name = models.CharField(max_length=50)
    # img = models.CharField(max_length=250, blank=True)
    short_description = models.CharField(max_length=250, blank=True)
    long_description = RichTextField(max_length=2500, blank=True)
    img = models.ImageField(upload_to='images/events/', blank=True)
    # state = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Artist(models.Model):
    name = models.CharField(max_length=100)
    short_description = models.CharField(max_length=500, blank=True)
    long_description = RichTextField(blank=True, max_length=2500)
    img = models.ImageField(upload_to='images/artists/', blank=True)
    # mp3 = models.CharField(max_length=50, blank=True)
    link_youtube = models.URLField(blank=True)
    link_spotify = models.URLField(blank=True)
    link_soundcloud = models.URLField(blank=True)
    link_bandcamp = models.URLField(blank=True)
    link_other = models.URLField(blank=True)
    user = models.ManyToManyField(User, related_name='artists')
    # state = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        permissions = (
            ('view_allartists', "Can view all artists"),
        )

# Signal handler for ManyToMany field changes on `user`
@receiver(m2m_changed, sender=Artist.user.through)
def delete_artist_if_no_users(sender, instance, action, **kwargs):
    if action == "post_remove":  # Trigger after users are removed
        if instance.user.count() == 0:  # Check if no users remain
            instance.delete()  # Delete the Artist instance

class Stage(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Program(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='programs')
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE, related_name='programs')
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='programs', blank=True, null=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='programs', blank=True, null=True)

    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True,blank=True)
    published = models.BooleanField(default=False)
    
    def __str__(self):
        if self.artist_id: 
            return f'{self.year} - {self.artist_id}'
        else: 
            return f'{self.year} - {self.event_id}'

class ApplicationStatus(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class ArtistApplication(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='artist_applications')
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='artist_applications')
    time = models.DateTimeField(auto_now_add=True)
    techrider = models.FileField(upload_to='techriders/', blank=True)
    user_comment = models.TextField(blank=True)

    status = models.ForeignKey(ApplicationStatus, on_delete=models.CASCADE, related_name='artist_applications')
    org_comment = RichTextField(blank=True)
    class Meta:
        permissions = (
            ('test_artistapplications', "Can test artist applications"),
            ('view_artistapplicationstatus','Can view artist application status'),
            ('change_artistapplicationstatus','Can change artist application status'),
            ('view_orgcomment', 'Can view artist application organiser comment'),
            ('change_orgcomment', 'Can change artist application organiser comment'),
        )

class Venue(models.Model):
    name = models.CharField(max_length=50)
    adress = models.CharField(max_length=50, blank=True)
    phone = models.CharField(max_length=16, blank=True)
    link = models.CharField(max_length=250, blank=True)

    def __str__(self):
        return self.name

class Concert(models.Model):
    time = models.DateTimeField()
    comment = RichTextField(max_length=250, blank=True)

    def __str__(self):
        return self.time

class Genre(models.Model):
    tag = models.CharField(max_length=50)

    def __str__(self):
        return self.tag

class ArtistManagement(models.Model):
    comment = models.TextField(max_length=250, blank=True)
    votes = models.IntegerField()
    stars = models.IntegerField()
    commentCount = models.IntegerField()

class VolunteerApplication(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='volunteer_application')
    time = models.DateTimeField(auto_now_add=True)
    area = models.ForeignKey("VolunteerArea", on_delete=models.CASCADE, related_name='volunteer_application')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='volunteer_application')
    user_comment = models.TextField(blank=True)
    manager = models.BooleanField(default=False) 
    status = models.ForeignKey(ApplicationStatus, on_delete=models.CASCADE, related_name='volunteer_application')
    org_comment = RichTextField(blank=True)

    def __str__(self):
        return f'{self.area} - {self.time}'
    
    class Meta:
        permissions = [
            ("view_volunteerapplicationstatus", "Can view volunteer application status"),
            ("change_volunteerapplicationstatus", "Can change volunteer application status"),
            ("view_orgcomment", "Can view organiser comment of volunteer application"),
            ("change_orgcomment", "Can change organiser comment of volunteer application"),
            ("change_volunteerapplicationarea", "Can change volunteer area for a volunteer application"),
        ]

class VolunteerArea(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='volunteer_area')
    name = models.CharField(max_length=50)
    published = models.BooleanField()
    mail = models.CharField(max_length=50, blank=True)
    volunteerCount = models.IntegerField(default=0)
    volunteerMax = models.IntegerField()
    managerCount = models.IntegerField(default=0)
    managerMax = models.IntegerField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    bg = models.ImageField(upload_to='images/areas/', blank=True)
    short_description = models.CharField(max_length=250, blank=True)
    long_description = RichTextField(max_length=10000, blank=True)

    def __str__(self):
        return self.name
    
    class Meta:
        permissions = [
            ("view_allareas", "Can view all volunteer areas"),
        ]
    
class TicketType(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='tickettype')
    name = models.CharField(max_length=50)
    price = models.CharField(max_length=50)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    description = models.CharField(max_length=500)
    stock = models.IntegerField()
    onlineSale = models.IntegerField()
    doorSale = models.IntegerField()
    published = models.BooleanField()

    def __str__(self):
        return f'{self.year} - {self.name}'
    
class Ticket(models.Model):
    ticketType = models.ForeignKey("TicketType", on_delete=models.CASCADE, related_name='ticket')
    purchase_date = models.DateField(max_length=50)

    def __str__(self):
        return f'{self.ticketType} - {self.purchase_date}'

class VolunteerShift(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='volunteershift', null=True, blank=True)
    area = models.ForeignKey(VolunteerArea, on_delete=models.CASCADE, related_name='volunteershift')
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    note = models.CharField(max_length=500)

    def __str__(self):
        return f'{self.area} - {self.start_time}'

    class Meta:
        permissions = [
            ("view_allvolunteershifts", "Can view all volunteer shifts"),
        ]
