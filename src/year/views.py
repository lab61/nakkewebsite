from django.views.generic import TemplateView, ListView, DetailView, UpdateView, CreateView, DeleteView, FormView 
from itertools import groupby
from operator import attrgetter
from django.shortcuts import redirect, get_object_or_404
from django.utils import timezone
from django.db.models import Q

from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from .models import Page, Donation, Year, VolunteerArea, Artist, Event, Program, ArtistApplication, Stage
from django.utils.http import urlencode
from django.contrib.auth.models import Group
from accounts.models import User, Committee, Mail
from year.forms import SendApplicationForm
from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from accounts.services import send_receiver_email

class IndexView(TemplateView):
    template_name = "year/home.html"
    context_object_name = 'year'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_year = Year.current_year()
        context['year'] = current_year

        page = Page.objects.get(id=41)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
        
        return context

class Praktisk_informationView(TemplateView):
    template_name = "year/information/praktisk_information.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=1).title
        context['bg'] = Page.objects.get(id=1).bg_image
        context['short_description'] = Page.objects.get(id=1).short_description
        context['long_description'] = Page.objects.get(id=1).long_description
       
        return context

class Bliv_frivilligView(ListView):
    template_name = "year/information/bliv_frivillig.html"
    context_object_name = 'areas'
    
    def get_queryset(self):
        current_year = Year.current_year()
        queryset = VolunteerArea.objects.filter(published=True).filter(year=current_year)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=2).title
        context['bg'] = Page.objects.get(id=2).bg_image
        context['short_description'] = Page.objects.get(id=2).short_description
        context['long_description'] = Page.objects.get(id=2).long_description
        
        current_year = Year.current_year()
        all_areas = VolunteerArea.objects.filter(published=True, year=current_year)
        open_areas = []
        closed_areas = []
        unopened_areas = []

        for area in all_areas:
            if area.volunteerMax == 0:
                unopened_areas.append(area)
            elif area.volunteerCount < area.volunteerMax:
                open_areas.append(area)
            elif area.volunteerCount >= area.volunteerMax:
                closed_areas.append(area)
        
        context['open_areas'] = open_areas
        context['closed_areas'] = closed_areas
        context['unopened_areas'] = unopened_areas

        return context

class Bliv_kunstnerView(TemplateView):
    template_name = "year/information/bliv_kunstner.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
    
        context['title'] = Page.objects.get(id=3).title
        context['bg'] = Page.objects.get(id=3).bg_image
        context['short_description'] = Page.objects.get(id=3).short_description
        context['long_description'] = Page.objects.get(id=3).long_description
        
        
        logged_in = self.request.user.is_authenticated
        if logged_in:
            artist = Artist.objects.filter(user=self.request.user).order_by('-pk').first()
        
        if not logged_in:
            context['next_url'] = reverse_lazy('accounts:signup') 
        if logged_in and artist:
            base_url = reverse_lazy('hzd:booking_create')
            query_params = {'artist_id': artist.id}
            context['next_url'] = f"{base_url}?{urlencode(query_params)}"
        if logged_in and not artist:
            context['next_url'] = reverse_lazy('hzd:artist_create') 

        return context

class Fallesbus_til_nakkeView(TemplateView):
    template_name = "year/information/fællesbus_til_nakke.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=4).title
        context['bg'] = Page.objects.get(id=4).bg_image
        context['short_description'] = Page.objects.get(id=4).short_description
        context['long_description'] = Page.objects.get(id=4).long_description
        
        return context

class Om_nakkefestivalView(TemplateView):
    template_name = "year/om/om_nakkefestival.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=5).title
        context['bg'] = Page.objects.get(id=5).bg_image
        context['short_description'] = Page.objects.get(id=5).short_description
        context['long_description'] = Page.objects.get(id=5).long_description
        
        return context

class HistorienView(TemplateView):
    template_name = "year/om/historien.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=6).title
        context['bg'] = Page.objects.get(id=6).bg_image
        context['short_description'] = Page.objects.get(id=6).short_description
        context['long_description'] = Page.objects.get(id=6).long_description
       
        return context

class Find_vejView(TemplateView):
    template_name = "year/om/find_vej.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=7).title
        context['bg'] = Page.objects.get(id=7).bg_image
        context['short_description'] = Page.objects.get(id=7).short_description
        context['long_description'] = Page.objects.get(id=7).long_description
       
        return context

class BillederView(TemplateView):
    template_name = "year/om/billeder.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=8).title
        context['bg'] = Page.objects.get(id=8).bg_image
        context['short_description'] = Page.objects.get(id=8).short_description
        context['long_description'] = Page.objects.get(id=8).long_description
       
        return context

class DonationerView(ListView):
    template_name = "year/om/donationer.html"
    context_object_name = 'years_with_donations'
    
    def get_queryset(self):
        return Year.objects.prefetch_related('donations').order_by('-year')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=9).title
        context['bg'] = Page.objects.get(id=9).bg_image
        context['short_description'] = Page.objects.get(id=9).short_description
        context['long_description'] = Page.objects.get(id=9).long_description
       
        return context

class VedtagterView(TemplateView):
    template_name = "year/om/vedtagter.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=10).title
        context['bg'] = Page.objects.get(id=10).bg_image
        context['short_description'] = Page.objects.get(id=10).short_description
        context['long_description'] = Page.objects.get(id=10).long_description
        
        return context

class PresseView(TemplateView):
    template_name = "year/om/presse.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=11).title
        context['bg'] = Page.objects.get(id=11).bg_image
        context['short_description'] = Page.objects.get(id=11).short_description
        context['long_description'] = Page.objects.get(id=11).long_description
       
        return context

class KontaktView(TemplateView):
    template_name = "year/om/kontakt.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=12).title
        context['bg'] = Page.objects.get(id=12).bg_image
        context['short_description'] = Page.objects.get(id=12).short_description
        context['long_description'] = Page.objects.get(id=12).long_description
       
        return context

class KunstnereView(ListView):
    template_name = "year/musik/kunstnere.html"
    context_object_name = 'years_with_artists'
    
    def get_queryset(self):
        year_param = self.kwargs.get('year')
        # Filter artists by ArtistApplications with the specified status and year_param
        artistApplications = ArtistApplication.objects.filter(status=2, year__year=year_param)
        return artistApplications
        # return Artist.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=13).title
        context['bg'] = Page.objects.get(id=13).bg_image
        context['short_description'] = Page.objects.get(id=13).short_description
        context['long_description'] = Page.objects.get(id=13).long_description
        context['year'] = self.kwargs.get('year')

        every_year = list(Year.objects.filter(artist_applications__artist__isnull=False).distinct().order_by('-year'))
        current_year = Year.current_year()

        if current_year not in every_year:
            every_year.insert(0,current_year)

        context['every_year'] = every_year
        
        artist_applications = context['object_list']  
        years_with_artists = [] 
        for artistApplication in context['years_with_artists']:
            # Set variables for all short_descriptions, long_descriptions and imgs from Artist
            artistApplication.artist_name = artistApplication.artist.name if artistApplication.artist else None
            artistApplication.artist_short_description = artistApplication.artist.short_description if artistApplication.artist else None
            artistApplication.artist_long_description = artistApplication.artist.long_description if artistApplication.artist else None
            artistApplication.artist_img = artistApplication.artist.img if artistApplication.artist else None
            years_with_artists.append(artistApplication)

        context['years_with_artists'] = years_with_artists
        
        return context

class ProgramView(ListView):
    template_name = "year/musik/program.html"
    context_object_name = 'years_with_programs'

    def get_queryset(self):
        # Get the selected year from the URL parameter
        # selected_year = self.kwargs.get('selected_year')
        year_param = self.kwargs.get('year')
        # queryset = Year.objects.filter(year=year_param).prefetch_related('programs').order_by('-year')
        queryset = Year.objects.all().prefetch_related('programs').order_by('-year')
        if year_param:
            queryset = queryset.filter(year=year_param)
        # if selected_year:
        #     # Filter programs based on the selected year
        #     queryset = get_object_or_404(Year, year=selected_year).programs.all().order_by('start_time')
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = Page.objects.get(id=14).title
        context['bg'] = Page.objects.get(id=14).bg_image
        context['short_description'] = Page.objects.get(id=14).short_description
        context['long_description'] = Page.objects.get(id=14).long_description
        context['year'] = self.kwargs.get('year')

        # year_param = self.kwargs.get('year')
        # context['year'] = year_param 
        years_with_programs = context['years_with_programs']
        
        for year in context['years_with_programs']:
            programs = year.programs.all().order_by('start_time')
            grouped_programs = {}
            for program in programs:
                if program.published == True:
                    # Set variables for all short_descriptions, long_descriptions and imgs from Artist and Event
                    program.artist_short_description = program.artist.short_description if program.artist else None
                    program.artist_long_description = program.artist.long_description if program.artist else None
                    program.artist_img = program.artist.img if program.artist else None
                    program.event_short_description = program.event.short_description if program.event else None
                    program.event_long_description = program.event.long_description if program.event else None
                    program.event_img = program.event.img if program.event else None
                    # Populate new array with Programs grouped by their date
                    date = program.start_time.date()
                    if date not in grouped_programs:
                        grouped_programs[date] = []
                    grouped_programs[date].append(program)
            year.grouped_programs = grouped_programs
        
        every_year = list(Year.objects.filter(programs__isnull=False).distinct().order_by('-year'))
        current_year = Year.current_year()

        if current_year not in every_year:
            every_year.insert(0,current_year)
        context['every_year'] = every_year

        return context

class EventsView(ListView):
    template_name = "year/musik/events.html"
    context_object_name = 'years_with_events'

    def get_queryset(self):
        year_param = self.kwargs.get('year')
        queryset = Year.objects.all().prefetch_related('programs__event').order_by('-year')
        if year_param:
            queryset = queryset.filter(year=year_param)
        return queryset
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=15).title
        context['bg'] = Page.objects.get(id=15).bg_image
        context['short_description'] = Page.objects.get(id=15).short_description
        context['long_description'] = Page.objects.get(id=15).long_description
        context['year'] = self.kwargs.get('year')
        
        # Fetch all years with non-null event_id programs
        every_year = list(Year.objects.filter(programs__event__isnull=False).distinct().order_by('-year'))
        current_year = Year.current_year()

        if current_year not in every_year:
            every_year.insert(0,current_year)
        context['every_year'] = every_year
        
        years_with_events = context['object_list']
        for year in years_with_events:
            events = {}
            for program in year.programs.all():
                if program.event:
                    event = program.event
                    if event:
                        if event not in events:
                            events[event] = []
                        events[event].append(program)
            year.events = events
        context['years_with_events'] = years_with_events

        return context

class CustomPermissionDeniedView(TemplateView):
    template_name = '403.html'
    context_object_name = 'permission_denied'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=1).title
        context['bg'] = Page.objects.get(id=1).bg_image
        context['short_description'] = Page.objects.get(id=1).short_description
        context['long_description'] = Page.objects.get(id=1).long_description
       
        return context

class SendApplicationView(FormView):
    template_name = "year/application_mail.html"
    form_class = SendApplicationForm
    
    def form_valid(self, form):
        receiver = "booking@nakkefestival.dk"
        message = "<dl>"
        start_title = "<dt>"
        end_title = "</dt>"
        start_item = "<dd>"
        end_item = "</dd>"
        for field_label, field_value in form.cleaned_data.items():
            if field_value:
                message += f"{start_title}{field_label}{end_title}{start_item}{field_value}{end_item}"
        message += "</dl>"

        uploaded_file = self.request.FILES.get('file')
        uploaded_image = self.request.FILES.get('image')
        
        artist_name = form.cleaned_data['name']
        print(f"Artist_name: {artist_name}")

        try:
            send_receiver_email(
                title=f"Kunstneransøgning: {artist_name}",
                receiver=receiver,
                mailId=22,
                message=message,
                attachments=[uploaded_file, uploaded_image]
            )
            print(f"Email sent to {receiver}!")
        except Exception as e:
            print(f"Error sending email: {e}")

        self.email = form.cleaned_data.get('email') # Pass email to get_success_url
        return super().form_valid(form)

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        if self.email:
            return reverse_lazy('accounts:application_sent', kwargs={'email': self.email})
        return reverse_lazy('year:home')

    def get_context_data(self, **kwargs):
        context = super(SendApplicationView, self).get_context_data(**kwargs)
        page = Page.objects.get(id=3)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['submit_button'] = 'Ansøg'

        return context

class PrivacyView(TemplateView):
    template_name = "year/om/privatlivspolitik.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = Page.objects.get(id=49)
        context['title'] = page.title
        context['bg'] = page.bg_image
        context['short_description'] = page.short_description
        context['long_description'] = page.long_description
       
        return context
