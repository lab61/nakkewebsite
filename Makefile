DOCKER_COMPOSE = COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose -p nakke -f docker/docker-compose.yml
DOCKER_RUN = ${DOCKER_COMPOSE} run -u `id -u`
MANAGE_EXEC = ${DOCKER_COMPOSE} exec django python /app/src/manage.py
MANAGE_RUN = ${DOCKER_RUN} django python /app/src/manage.py

run:
	${DOCKER_COMPOSE} up

build:
	${DOCKER_COMPOSE} build --no-cache

migrate:
	${MANAGE_RUN} $@ ${ARGS}

makemigrations:
	${MANAGE_RUN} $@ ${ARGS}

bootstrap_devsite:
	${MANAGE_RUN} $@ ${ARGS}

shell:
	${MANAGE_RUN} $@ ${ARGS}

manage:
	${MANAGE_RUN} ${COMMAND}

createsuperuser:
	${MANAGE_RUN} $@ ${ARGS}

clean:
#	rm -rf volumes/
#	rm src/year/migrations/00*
#	rm src/accounts/migrations/00*

	# if [ -e volumes ]; then
	# 	echo "volumes will be removed"
		# rm -rf volumes/
	# else
	# 	echo "volumes do not exist"
	# fi

	# if [ -e src/year/migrations/00* ]; then
	# 	echo "00* will be removed"
	# 	rm src/year/migrations/00*
	# else
	# 	echo "00* does not exist"
	# fi

bootstrap: clean makemigrations migrate
	echo "Create Super User:"
	make manage COMMAND="createsuperuser"
	#	make manage COMMAND="loaddata committee"
	make manage COMMAND="loaddata year"
	make manage COMMAND="loaddata donation"
	make manage COMMAND="loaddata event"
	make manage COMMAND="loaddata pages"
	make manage COMMAND="loaddata stage"
	make manage COMMAND="loaddata volunteerarea"
	make manage COMMAND="loaddata applicationstatus"
	make manage COMMAND="loaddata tickettype"
	make manage COMMAND="loaddata artist"
	make manage COMMAND="loaddata program"
	make manage COMMAND="loaddata volunteerapplication"
	make manage COMMAND="loaddata artistapplication"
	make manage COMMAND="loaddata group"
	make manage COMMAND="loaddata mail"
